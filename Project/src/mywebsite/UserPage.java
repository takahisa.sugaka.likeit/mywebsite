package mywebsite;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.RestaurantDataBeans;
import beans.UserDataBeans;
import dao.ReserveDAO;
import dao.RestaurantDAO;
import dao.UserDAO;

/**
 * Servlet implementation class UserData
 */
@WebServlet("/UserPage")
public class UserPage extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserPage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		try {
			//セッションからuserIdを取得
			int userId = (int)session.getAttribute("userId");
			//更新画面からもどってきた場合、セッションから取得。それ以外はuserIDでユーザーを取得
			UserDataBeans udb = session.getAttribute("returnUDB") == null ? UserDAO.getUserDataBeansByUserId(userId) : (UserDataBeans) WebHelper.cutSessionAttribute(session, "returnUDB");

			//エラーメッセージがあったときにエラーメッセージを格納する
			String validationMessage = (String) WebHelper.cutSessionAttribute(session, "validationMessage");
			request.setAttribute("validationMessage", validationMessage);

			//最近行ったレストランのidをListの形で取得する
			List<Integer> recentRestaurantId = ReserveDAO.getRecentReserveByUserId(userId);
			if(recentRestaurantId.size() != 0) {
				//Listから重複するレストランidを削除する
				List<Integer>hashRecentRestaurantId = new ArrayList<Integer>(new LinkedHashSet<>(recentRestaurantId));
				//RestaurantDataBeansのListを初期化
				ArrayList<RestaurantDataBeans> userRecentReserveList = new ArrayList<RestaurantDataBeans>();


				//表示件数は3件
				if(hashRecentRestaurantId.size() < 3) {
					for(int i = 0; i < hashRecentRestaurantId.size(); i++) {
						//Integer型のレストランidをint型に変換
						Integer BeforeConversion = hashRecentRestaurantId.get(i);
						int restaurantId = BeforeConversion.intValue();
						//変換したidからレストランのBeansを取得
						userRecentReserveList.add(RestaurantDAO.getRestaurantById(restaurantId));
					}
				}else {
					for(int i = 0; i < 3; i++) {
						//Integer型のレストランidをint型に変換
						Integer BeforeConversion = hashRecentRestaurantId.get(i);
						int restaurantId = BeforeConversion.intValue();
						//変換したidからレストランのBeansを取得
						userRecentReserveList.add(RestaurantDAO.getRestaurantById(restaurantId));
					}
				}

				request.setAttribute("recentReserve", userRecentReserveList);
			}





			//ユーザー情報をリクエスト領域にセット
			request.setAttribute("udb", udb);

			request.getRequestDispatcher(WebHelper.USER_PAGE).forward(request, response);
		}catch(Exception e){
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}

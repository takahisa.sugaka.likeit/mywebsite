package mywebsite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.RestaurantDataBeans;
import dao.RestaurantDAO;

/**
 * Servlet implementation class RestaurantDelete
 */
@WebServlet("/RestaurantDeleteConfirm")
public class RestaurantDeleteConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RestaurantDeleteConfirm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		// マルチバイト文字列の文字化け対応
		request.setCharacterEncoding("UTF-8");
		try {
			//セッションからユーザーidを取得
			int userId = (int)session.getAttribute("userId");

			//管理者アカウントでログインしていなかったらリダイレクト
			if(userId != WebHelper.ADMIN_ID) {
				response.sendRedirect("Index");
				return;
			}

			String restaurantIdS = request.getParameter("id");
			int restaurantId = Integer.parseInt(restaurantIdS);
			session.setAttribute("restaurantId", restaurantId);
			RestaurantDataBeans rdb = RestaurantDAO.getRestaurantById(restaurantId);

			request.setAttribute("rdb", rdb);
			request.getRequestDispatcher(WebHelper.RESTAURANT_DELETE_CONFIRM).forward(request, response);
		}catch(Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

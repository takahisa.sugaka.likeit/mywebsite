package mywebsite;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ReserveDataBeans;
import dao.ReserveDAO;

/**
 * Servlet implementation class ReserveList
 */
@WebServlet("/ReserveList")
public class ReserveList extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReserveList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {

			//セッションからユーザーidを取得
			int userId = (int)session.getAttribute("userId");

			//予約日時が今日以降の予約を取得
			ArrayList<ReserveDataBeans> reserveList = ReserveDAO.getReserveListByUserId(userId);
			//予約日時が今日より前のものと予約キャンセルしたものを取得
			ArrayList<ReserveDataBeans> pastReserveList = ReserveDAO.getPastReserveListByUserId(userId);
			//取得した情報をリクエスト領域にセット
			request.setAttribute("rl", reserveList);
			request.setAttribute("prl", pastReserveList);

			request.getRequestDispatcher(WebHelper.RESERVE_LIST_PAGE).forward(request, response);
		}catch(Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

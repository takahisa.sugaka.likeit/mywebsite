package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import base.DBManager;
import beans.UserDataBeans;
import mywebsite.WebHelper;

public class UserDAO {

	public static int getUserId(String loginId, String password) throws SQLException{
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM user WHERE login_id = ?");
			st.setString(1, loginId);

			ResultSet rs = st.executeQuery();

			int userId = 0;
			while(rs.next()) {
				if(WebHelper.encryptionPassword(password).equals(rs.getString("login_password"))) {
					userId = rs.getInt("id");
					System.out.println("login succeeded");
					break;
				}
			}
			System.out.println("searching userId by loginId has been completed");
			return userId;
		}catch(SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if(con != null) {
				con.close();
			}
		}
	}

	public static boolean isOverLapLoginId(String loginId, int userId) throws SQLException {
		// 重複しているかどうか表す変数
		boolean isOverlap = false;

		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			// 入力されたlogin_idが存在するか調べる
			st = con.prepareStatement("SELECT login_id FROM user WHERE login_id = ? AND id != ?");
			st.setString(1, loginId);
			st.setInt(2, userId);
			ResultSet rs = st.executeQuery();

			System.out.println("searching loginId by inputLoginId has been completed");

			if (rs.next()) {
				isOverlap = true;
			}
		}catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

		return isOverlap;
	}

	public static boolean isOverLapMailAddress(String mailAddress, int userId) throws SQLException {
		// 重複しているかどうか表す変数
		boolean isOverlap = false;

		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			// 入力されたmailAddressが存在するか調べる
			st = con.prepareStatement("SELECT mail_address FROM user WHERE mail_address = ? AND id != ?");
			st.setString(1, mailAddress);
			st.setInt(2, userId);
			ResultSet rs = st.executeQuery();
			if (rs.next()) {
				isOverlap = true;
			}

		}catch(SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if (con != null) {
				con.close();
			}
		}
		System.out.println("overlap check has been completed");
		return isOverlap;

	}

	public static void insertUser(UserDataBeans udb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO user(name, login_id, mail_address, login_password, create_date) values(?, ?, ?, ?, now())");
			st.setString(1, udb.getName());
			st.setString(2, udb.getLoginId());
			st.setString(3, udb.getMailAddress());
			st.setString(4, WebHelper.encryptionPassword(udb.getPassword()));
			st.executeUpdate();
			System.out.println("inserting user has been completed");
		}catch(SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static UserDataBeans getUserDataBeansByUserId(int userId) throws SQLException{
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT id, name, login_id, mail_address FROM user where id =?");
			st.setInt(1, userId);
			ResultSet rs = st.executeQuery();

			UserDataBeans udb = new UserDataBeans();

			if(rs.next()) {
				udb.setId(rs.getInt("id"));
				udb.setName(rs.getString("name"));
				udb.setLoginId(rs.getString("login_id"));
				udb.setMailAddress(rs.getString("mail_address"));
			}
			System.out.println("searching userDataBeans by loginId has been completed");
			return udb;

		}catch(SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if (con != null) {
				con.close();
			}
		}
}

	public static void updateUser(UserDataBeans udb) throws SQLException{
		Connection con = null;
		PreparedStatement st = null;

		try {con = DBManager.getConnection();
		st = con.prepareStatement("UPDATE user SET name = ?, login_id = ?, mail_address = ? WHERE id = ?");
		st.setString(1, udb.getName());
		st.setString(2, udb.getLoginId());
		st.setString(3, udb.getMailAddress());
		st.setInt(4, udb.getId());
		st.executeUpdate();
		System.out.println("update has been completed");


		}catch(SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if (con != null) {
				con.close();
			}
		}

	}

	public static void userDelete(int userId) throws SQLException{
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("DELETE FROM user WHERE id = ?");
			st.setInt(1, userId);
			st.executeUpdate();
		}catch(SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if (con != null) {
				con.close();
			}
		}
	}

}

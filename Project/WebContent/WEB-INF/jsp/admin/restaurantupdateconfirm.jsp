<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>レストラン情報更新確認</title>
        <link rel="stylesheet"
              href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">
        <link href="css/style.css" rel="stylesheet" type="text/css" />
    </head>

    <body>
       <jsp:include page="/baselayout/admin_header.jsp" />

        <div class="container">
            <div class="row mt-4">
                <h4 class="col-md-12 text-center reservelist-header">レストラン情報更新確認</h4>
            </div>
	        <div class="row">
	            <div class="col-md-12 reserve-result">
	                <table class="table table-bordered">
			            <tr>
					        <th>レストラン名</th>
					        <td>${restdb.name}</td>
			            </tr>
			            <tr>
					        <th>エリア</th>
					        <td>${restdb.areaName}</td>
			            </tr>
			            <tr>
					        <th>電話番号</th>
					        <td>${restdb.phoneNumber}</td>
			            </tr>
			            <tr>
					        <th>住所</th>
					        <td>${restdb.address}</td>
						</tr>
						<tr>
							<th>写真</th>
		 					<td><!--<input type="file" class="form-control"　readonly>--></td>
	 					</tr>
						<tr>
							<th>レストランPR</th>
							<td>${restdb.pr}</td>
						</tr>
						<tr>
							<th>レストラン詳細</th>
							<td>${restdb.detail}</td>
						</tr>
	                </table>
	            </div>
	        </div>
				<form method="POST" action="RestaurantUpdateResult">
	            <div class="row mt-5">
		           	<div class="col-md-6 col-sm-6">
						<button class="btn btn-primary col-md-4 offset-md-4" type="submit" name="confirmButton" value="cancel">キャンセル</button>
					</div>
		            <div class="col-md-6 col-sm-6">
		                <button class="btn btn-danger col-md-4 offset-md-4" type="submit" name="confirmButton" value="update">更新する</button>
					</div>
	            </div>
            </form>

        </div>
    </body>
</html>
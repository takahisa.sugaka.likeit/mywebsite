package mywebsite;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.RestaurantDataBeans;
import dao.AreaDAO;
import dao.RestaurantDAO;

/**
 * Servlet implementation class SearchResult
 */
@WebServlet("/SearchResult")
public class SearchResult extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			//入力された文字を変数に代入する
			String area = request.getParameter("area");
			String keyWord = request.getParameter("keyword");

			//レストランを格納するリストを定義
			ArrayList<RestaurantDataBeans> restaurantSearchResult = null;
			//総ページ数取得のための変数
			double restCount = 0;
			int pageMax = 0;

			//表示ページ番号 未指定の場合 1ページ目を表示
			int pageNum = Integer.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));

			//検索エリアと検索ワードをセッションに格納する。
			session.setAttribute("keyword", keyWord);
			session.setAttribute("area", area );

			int areaId = 0;
			//エリアが入力されているかを確認
			if(!area.equals("")) {
				areaId = AreaDAO.getAreaId(area);
			}

			//エリアIDが正の整数の時、エリアIDと名前でレストランを検索
			if(!(areaId == 0) && !(areaId == -1)) {
				//レストラン検索メソッドを実行
				restaurantSearchResult = RestaurantDAO.getRestaurantByNameAndId(keyWord, areaId, pageNum, WebHelper.PAGE_MAX_ITEM_COUNT);
				// レストランの総数を取得
				restCount = RestaurantDAO.getRestaurantCountById(keyWord, areaId);
			}

			if(areaId == 0) {
				//レストラン検索メソッドを実行
				restaurantSearchResult = RestaurantDAO.getRestaurantByName(keyWord, pageNum, WebHelper.PAGE_MAX_ITEM_COUNT);
				// レストランの総数を取得
				restCount = RestaurantDAO.getRestaurantCountByKeyWord(keyWord);
			}

			if(areaId == -1){
				request.setAttribute("areaId", areaId);
				String message = "そのエリアは登録されていません。";
				request.setAttribute("message", message);
			}

			//検索ワードに対する総ページ数を取得
			pageMax = (int) Math.ceil(restCount/WebHelper.PAGE_MAX_ITEM_COUNT);

			//総アイテム数
			request.setAttribute("itemCount", (int) restCount);
			// 総ページ数
			request.setAttribute("pageMax", pageMax);
			// 表示ページ
			request.setAttribute("pageNum", pageNum);
			request.setAttribute("restaurantList", restaurantSearchResult);

			request.getRequestDispatcher(WebHelper.SEARCH_RESULT).forward(request, response);
		}catch(Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}

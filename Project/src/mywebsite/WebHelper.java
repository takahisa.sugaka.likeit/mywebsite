package mywebsite;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import javax.xml.bind.DatatypeConverter;

public class WebHelper {
	//画面遷移用に定数化したURL
	static final String LOGIN_PAGE = "/WEB-INF/jsp/website/login.jsp";
	static final String TOP_PAGE = "/WEB-INF/jsp/website/index.jsp";
	static final String SEARCH_RESULT = "/WEB-INF/jsp/website/restaurantsearchresult.jsp";
	public static final String RESTAURANT_PAGE = "/WEB-INF/jsp/website/restaurant.jsp";
	public static final String LOGOUT_PAGE = "/WEB-INF/jsp/website/logout.jsp";
	public static final String RESERVE_CONFIRM = "WEB-INF/jsp/website/reserveconfirm.jsp";
	public static final String RESERVE_RESULT_PAGE = "WEB-INF/jsp/website/reserveresult.jsp";
	public static final String REGIST_PAGE = "WEB-INF/jsp/website/regist.jsp";
	public static final String REGIST_CONFIRM_PAGE ="WEB-INF/jsp/website/registconfirm.jsp";
	public static final String REGIST_RESULT_PAGE = "WEB-INF/jsp/website/registresult.jsp";
	public static final String USER_PAGE = "WEB-INF/jsp/website/userpage.jsp";
	public static final String USER_UPDATE_CONFIRM_PAGE = "WEB-INF/jsp/website/userupdateconfirm.jsp";
	public static final String USER_UPDATA_RESULT_PAGE = "WEB-INF/jsp/website/userupdateresult.jsp";
	public static final String RESERVE_LIST_PAGE = "WEB-INF/jsp/website/reservelist.jsp";
	public static final String RESERVE_DETAIL_PAGE = "WEB-INF/jsp/website/reservedetail.jsp";
	public static final String CANCEL_CONFIRM_PAGE = "WEB-INF/jsp/website/reservecancelconfirm.jsp";
	public static final String CANCEL_CONFIRM_RESULT_PAGE = "WEB-INF/jsp/website/reservecancelresult.jsp";
	public static final String ERROR_PAGE = "WEB-INF/jsp/website/error.jsp";
	public static final String USER_DELETE = "WEB-INF/jsp/website/userdelete.jsp";
	public static final String USER_DELETE_RESULT_PAGE = "WEB-INF/jsp/website/userdeleteresult.jsp";

	//管理画面のURL
	public static final String ADMIN_INDEX = "WEB-INF/jsp/admin/admin.jsp";
	public static final String NEW_REGISTER = "WEB-INF/jsp/admin/newregistration.jsp";
	public static final String RESTAURANT_REGISTRATION_CONFIRM = "WEB-INF/jsp/admin/registrationconfirm.jsp";
	public static final String RESTAURANT_REGISTRATION_RESULT = "WEB-INF/jsp/admin/registrationresult.jsp";
	public static final String RESTAURANT_DETAIL = "WEB-INF/jsp/admin/restaurantdetail.jsp";
	public static final String RESTAURANT_UPDATE = "WEB-INF/jsp/admin/restaurantupdate.jsp";
	public static final String RESTAURANT_UPDATE_CONFIRM = "WEB-INF/jsp/admin/restaurantupdateconfirm.jsp";
	public static final String RESTAURANT_UPDATE_RESULT = "WEB-INF/jsp/admin/restaurantupdateresult.jsp";
	public static final String RESTAURANT_DELETE_CONFIRM = "WEB-INF/jsp/admin/restaurantdeleteconfirm.jsp";
	public static final String RESTAURANT_DELETE_RESULT = "WEB-INF/jsp/admin/restaurantdeleteresult.jsp";


	//管理者ID
	public static final int ADMIN_ID = 1;
	public static final int PAGE_MAX_ITEM_COUNT = 5;

	public static Object cutSessionAttribute(HttpSession session, String str) {
		Object test = session.getAttribute(str);
		session.removeAttribute(str);
		return test;
	}

	//パスワード暗号化メソッド
	public static String encryptionPassword(String password){
			try {
				//ハッシュを生成したい元の文字列
				String source = password;
				//ハッシュ生成前にバイト配列に置き換える際のCharset
				Charset charset = StandardCharsets.UTF_8;
				//ハッシュアルゴリズム
				String algorithm = "MD5";

				//ハッシュ生成処理
				byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
				String hPassword = DatatypeConverter.printHexBinary(bytes);
				return hPassword;
			}catch(NoSuchAlgorithmException e){
				e.printStackTrace();
				return null;
			}

		}


	public static boolean isLoginIdValidation(String loginId) {
		//英数字アンダースコア以外が入力されているかをチェック
		if(loginId.matches("[0-9a-zA-A_]+")) {
			return true;
		}
		return false;

	}


	public static boolean isMailAdressValidation(String mailAddress) {
		if(mailAddress.matches("^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-]*$")){
			System.out.println(mailAddress + "はメールアドレスです");
			return true;
		}else if(mailAddress.matches("^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-]+\\.[a-zA-Z0-9-]*$")) {
			System.out.println(mailAddress + "はメールアドレスです");
			return true;
		}
		System.out.println(mailAddress + "はメールアドレスではありません");
		return false;

	}

	//アップロードされた画像から名前を抽出するメソッド
	public static String getFileName(Part part) {
		String name = null;
		for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
			if (dispotion.trim().startsWith("filename")) {
				name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
				name = name.substring(name.lastIndexOf("\\") + 1);
				break;
			}
		}
		return name;
	}
	//アップロードされた画像をtmpからimgに移動するメソッド
	public static String moveFile(String fileName)throws  IOException {
		Path movedFile = Paths.get("/Users/Takahisa/Documents/git/MyWebSite/Project/WebContent/tmp", fileName);
        Path destinationFile = Paths.get("/Users/Takahisa/Documents/git/MyWebSite/Project/WebContent/img", fileName);

		try {
			Files.move( movedFile, destinationFile);
			return fileName;
		} catch (Exception e) {
			e.printStackTrace();
			return e.toString();
		}
	}
}

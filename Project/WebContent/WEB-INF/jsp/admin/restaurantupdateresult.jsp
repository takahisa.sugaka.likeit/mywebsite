<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>レストラン情報更新完了</title>
        <link rel="stylesheet"
              href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">
        <link href="css/admin_style.css" rel="stylesheet" type="text/css" />
    </head>

    <body>
        <jsp:include page="/baselayout/admin_header.jsp" />

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="card reserve-confirm col-md-12">
                        <div class="card-content">
                            <div class="row">
                                <div class="mx-auto">
                                    <h4 class="justify-content-center mt-2">
                                        レストラン情報を更新しました
                                    </h4>
                                </div>
                            </div>
                            <div class="row text-center">
                                <div class="col-md-12">
                                    <a class="btn btn-reserve btn-danger" href="Admin">TOPへ戻る</a>
                                </div>
                             </div>
                        </div>
				    </div>
                </div>
            </div>
        </div>
    </body>
</html>
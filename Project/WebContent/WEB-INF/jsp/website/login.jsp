<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ログイン</title>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
   	<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<jsp:include page="/baselayout/header.jsp" />

    <div class="container">


        <div class="row">

        	<div class="col-md-12">
        		<div class="login-content">
                    <div class="login-page">
                        <div class="login">
                        	 <div class="row mx-auto">
					        	<div class="col-md-12">
						    		<c:if test="${loginErrorMessage != null}">
										<p class="text-danger mx-auto">${loginErrorMessage}</p>
										<br>
									</c:if>
								</div>
					    	</div>
                            <form class="login-form" action="LoginResult" method="post">
                                <input type="text" placeholder="ログインID" name="login_id" required/>
                                <input type="password" placeholder="パスワード" name="password" required/>
                                <button>ログイン</button>
                                <p class="message"><a href="Regist">新規登録</a>はこちらから</p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</body>
</html>
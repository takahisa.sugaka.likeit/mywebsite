<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>マイページ</title>
        <link rel="stylesheet"
              href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">
        <link href="css/style.css" rel="stylesheet" type="text/css" />
    </head>

    <body>
        <jsp:include page="/baselayout/header.jsp" />

        <div class="container">
        	<div class="row">
	        	<div class="col-md-12">
		            <div class="row mt-4">
		                <h4 class="col-md-12 text-center reservelist-header">ユーザー情報</h4>
		            </div>
	            </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card reserve-confirm mb-5">
                        <div class="card-content">
                            <div class="form-group">
                                <form action="UserUpdateConfirm" method="POST">
                                	<c:if test="${validationMessage != null}">
										<P class="text-danger mx-auto">${validationMessage}</P>
									</c:if>
                                    <div class="row  mt-1">
                                        <div class="col-md-6 col-sm-6">
                                            <label class="ml-1">名前</label>
                                            <input type="text" name="user_name" value="${udb.name}" class="form-control">
                                        </div>
                                        <div class=" col-md-6 col-sm-6">
                                            <label class="ml-1">ログインID</label>
                                            <input type="text" name="login_id" value="${udb.loginId}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <label class="ml-1">メールアドレス</label>
                                            <input type="email" name="user_mail_address" value="${udb.mailAddress}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 mt-3">
                                            <button class="btn btn-success col-md-4 offset-md-4" type="submit" name="action">更新</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--最近行ったお店の履歴-->
            <div class="row">
	        	<div class="col-md-12">
		            <div class="row mt-4">
		                <h4 class="col-md-12 text-center reservelist-header">最近行ったレストラン</h4>
		            </div>
	            </div>
            </div>
            <div class="row">
            	<div class="col-md-12">
	            	<div class="restaurants-area mt-4">
                    	<div class="card-deck mt-1">
                    	<c:forEach var="rest" items="${recentReserve}">
                    		<div class="card" style="width: 20rem;">
			                    <img class="card-img-top" src="img/${rest.fileName}" alt="Card image cap">
			                    <div class="card-body">
				                    <h4 class="card-title">${rest.name}</h4>
				                    <p class="card-text">${rest.pr}</p>
				                    <a href="Restaurant?restaurant_id=${rest.id}" class="btn btn-orange">詳細へ</a>
			                    </div>
		                    </div>
                    	</c:forEach>
		            	</div>
	            	</div>
	            </div>
	        </div>
			<div class="row mt-3">
		    	<div class="col-offset-8 col-md-2text-right  mt-5 mr-1">
		        	<a href="UserDelete">退会する</a>
		    	</div>
			</div>
		</div>
    </body>
</html>
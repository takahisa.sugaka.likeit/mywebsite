
package mywebsite;

import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ReserveDataBeans;
import dao.ReserveDAO;



/**
 * Servlet implementation class ReserveResult
 */
@WebServlet("/ReserveResult")
public class ReserveResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReserveResult() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {
			//セッションから予約情報Beansを取得
			ReserveDataBeans rdb = (ReserveDataBeans) WebHelper.cutSessionAttribute(session, "rdb") ;
			//予約のための日付を取得
			String reserveDate = (String) WebHelper.cutSessionAttribute(session, "inputReserveDate");

			//予約情報をDBに登録
			ReserveDAO.insertReserve(rdb, reserveDate);

			// フォーマット変換用
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");

			//変換した日付を改めてinputReserveDateに代入
			String formatReserveDate = sdf.format(rdb.getReserveDate());
			//inputDataの値をリクエスト領域に代入
			request.setAttribute("reserveDate", formatReserveDate);
			request.setAttribute("reserveTime", rdb.getReserveTime());
			request.setAttribute("numPeople", rdb.getnumPeople());
			request.getRequestDispatcher(WebHelper.RESERVE_RESULT_PAGE).forward(request, response);


		}catch(Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}


<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
　<html>
	<head>
	    <meta charset="UTF-8">
	    <title>予約内容詳細</title>
	    <link rel="stylesheet"
	          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	          crossorigin="anonymous">
	    <link href="css/style.css" rel="stylesheet" type="text/css" />
	</head>

	<body>
	    <jsp:include page="/baselayout/header.jsp" />

	    <div class="container">
	        <div class="row">
	            <div class="col-md-12 mt-3">
	                <div class="reservelist-header-area">
	                    <h4 class="reservelist-header text-center">
	                        予約内容詳細
	                    </h4>
	                </div>
	            </div>
	        </div>
	        <div class="row">
				<div class="col-md-12">
					<div class="card reserve-confirm col-md-12">
						<div class="card-content">
							<form method="POST" action="ReserveCancelConfirm">
								<div class="row">
									<table class="table table-bordered">
										<thead>
											<tr>
	                                            <th class="text-center" style="width: 20%;">店舗情報</th>
	                                            <th class="text-center" style="width: 20%;">アクセス</th>
												<th class="text-center" style="width: 20%;">来店日</th>
												<th class="text-center" style="width: 20%;">時間</th>
												<th class="text-center" style="width: 20%;">人数</th>
											</tr>
										</thead>
										<tbody>
									        <tr>
	                                            <td class="text-center">${rdb.restaurantName}</td>
	                                            <td class="text-center">${rdb.restaurantAddress}</td>
	                                            <td class="text-center">${rdb.formatDate}</td>
	                                            <td class="text-center">${rdb.reserveTime}</td>
	                                            <td class="text-center">${rdb.numPeople}人</td>
									        </tr>
										</tbody>
									</table>
								</div>
								<div class="row text-center">
									<c:choose>
										<c:when test="${rdb.cancel == true || dateComparison == false}">
											<div class="col-md-12">
												<button class="btn btn-secondary return-reservelist" type="submit" name="confirmButton" value="return">予約一覧へ</button>
											</div>

										</c:when>
										<c:otherwise>
	                                    	 <div class="col-md-6">
												<button class="btn btn-secondary return-reservelist" type="submit" name="confirmButton" value="return">予約一覧へ</button>
											</div>
											<div class="col-md-6 col-sm-6">
												<input type="hidden" name="reserve_id" value="${rdb.id}">
												<button class="btn btn-reserve btn-danger" type="submit" name="confirmButton" value="cancel">予約キャンセル</button>
											</div>
	                                    </c:otherwise>
									</c:choose>
								</div>
							</form>
						</div>
					</div>
				</div>
	        </div>
	    </div>
	</body>
</html>
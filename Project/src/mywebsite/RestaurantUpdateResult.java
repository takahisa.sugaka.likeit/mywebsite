package mywebsite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.RestaurantDataBeans;
import dao.RestaurantDAO;

/**
 * Servlet implementation class RestaurantUpdateResult
 */
@WebServlet("/RestaurantUpdateResult")
public class RestaurantUpdateResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RestaurantUpdateResult() {
        super();
        // TODO Auto-generated constructor stub
    }
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {
			//セッションからレストラン情報を取得
			RestaurantDataBeans restdb = (RestaurantDataBeans) session.getAttribute("restdb");
			//押されたボタンを確認する
			String confirmed = request.getParameter("confirmButton");
			//押されたボタンによって処理が分岐
			switch(confirmed) {
			//戻るボタンが押されていたら画面へ戻る
			case "cancel":
				String restaurantIdS = String.valueOf(restdb.getAreaId());
				session.setAttribute("restaurantIdS", restaurantIdS);
				response.sendRedirect("RestaurantUpdate");
				break;
			//更新ボタンが押されていたら、レストラン情報を削除して完了画面へ遷移
			case "update":
				//アップデートを実行
				RestaurantDAO.restauranUpdate(restdb);
				System.out.println(restdb.getFileName());
				//画像移動用の変数を初期化
				String fileName = "";
				String moveFile = "";
				//画像がアップロードされていた時の処理
				if(restdb.getFileName() != null){
					fileName = restdb.getFileName();
					moveFile = WebHelper.moveFile(fileName);
				}

				//画像の移動でエラーが起こった時にエラー画面に遷移するための処理
				if(!moveFile.equals(fileName)) {
					session.setAttribute("errorMessage", moveFile);
					response.sendRedirect("Error");
				}
				session.removeAttribute("restdbU");
				request.getRequestDispatcher(WebHelper.RESTAURANT_UPDATE_RESULT).forward(request, response);
			}
		}catch(Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}

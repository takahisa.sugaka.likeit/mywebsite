package mywebsite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import beans.RestaurantDataBeans;
import dao.AreaDAO;

/**
 * Servlet implementation class RestaurantUpdateConfirm
 */
@WebServlet("/RestaurantUpdateConfirm")
@MultipartConfig(location = "/Users/Takahisa/Documents/git/MyWebSite/Project/WebContent/tmp", maxFileSize = 1048576)
public class RestaurantUpdateConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RestaurantUpdateConfirm() {
        super();
        // TODO Auto-generated constructor stub
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {
			//セッションからユーザーidを取得
			int userId = (int)session.getAttribute("userId");

			//管理者アカウントでログインしていなかったらリダイレクト
			if(userId != WebHelper.ADMIN_ID) {
				response.sendRedirect("Index");
				return;
			}

			//RestaurantDataBeanに入れたいものの値を適宜変換しておく
			int areaId = Integer.parseInt(request.getParameter("area_id"));
			int id = Integer.parseInt(request.getParameter("id"));
			String areaName = AreaDAO.getAreaNameById(areaId);

			RestaurantDataBeans restdb = new RestaurantDataBeans();
			//partインスタンスを生成
			Part part = request.getPart("photo");
			String photoName = WebHelper.getFileName(part);
			//画像がアップロードされていた時の処理
			if(!photoName.equals("")) {
				part.write(photoName);
				restdb.setFileName(photoName);
				System.out.println("画像がアップロードされています");
			}

			//beansに各種値をセットする
			restdb.setName(request.getParameter("restaurant_name"));
			restdb.setPhoneNumber(request.getParameter("phone_number"));
			restdb.setAddress(request.getParameter("address"));
			restdb.setPr(request.getParameter("pr"));
			restdb.setDetail(request.getParameter("detail"));
			restdb.setAreaId(areaId);
			restdb.setAreaName(areaName);
			restdb.setId(id);

			session.setAttribute("restdb", restdb);
			request.getRequestDispatcher(WebHelper.RESTAURANT_UPDATE_CONFIRM).forward(request, response);
		}catch(Exception e) {

		}
	}
}

package mywebsite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;


/**
 * Servlet implementation class RegistResult
 */
@WebServlet("/RegistResult")
public class RegistResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegistResult() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			String inputUserName = request.getParameter("user_name");
			String inputLoginId = request.getParameter("login_id");
			String inputMailAddress = request.getParameter("user_mail_address");
			String inputPassword = request.getParameter("password");

			//ユーザー情報のBeansを作成
			UserDataBeans udb = new UserDataBeans();
			udb.setName(inputUserName);
			udb.setLoginId(inputLoginId);
			udb.setMailAddress(inputMailAddress);
			udb.setPassword(inputPassword);

			//registconfirm.jspで押されたボタンのvalueを取得
			String confirmed =request.getParameter("confirm_button");
			switch(confirmed) {
			//修正ボタンが押されていた場合
			case "cancel":
				session.setAttribute("udb", udb);
				response.sendRedirect("Regist");
				break;

			//登録ボタンが押されていたとき
			case "regist":
				UserDAO.insertUser(udb);
				request.setAttribute("udb", udb);
				request.getRequestDispatcher(WebHelper.REGIST_RESULT_PAGE).forward(request, response);
				break;
			}


		}catch(Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}


	}

}

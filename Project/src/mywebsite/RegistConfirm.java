package mywebsite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;


@WebServlet("/RegistConfirm")
public class RegistConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegistConfirm() {
        super();
        // TODO Auto-generated constructor stub
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* 文字化け対策 */
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			String inputUserName = request.getParameter("user_name");
			String inputLoginId = request.getParameter("login_id");
			String inputMailAddress = request.getParameter("user_mail_address");
			String inputMailAddressConfirm = request.getParameter("user_mail_address_confirm");
			String inputPassword = request.getParameter("password");
			String inputPasswordConfirm = request.getParameter("password_confirm");

			//ユーザー情報のBeansを作成
			UserDataBeans udb = new UserDataBeans();
			udb.setName(inputUserName);
			udb.setLoginId(inputLoginId);
			udb.setMailAddress(inputMailAddress);
			udb.setPassword(inputPassword);
			//エラーメッセージを初期化
			String validationMessage = "";

			//メールアドレスと確認用のメールアドレスが同じものかをチェック
			if(!inputMailAddress.equals(inputMailAddressConfirm)) {
				validationMessage += "入力されているメールアドレスと確認用メールアドレスが違います<br>";
			}

			//パスワードと確認用パスワードが同じものかをチェック
			if(!inputPassword.equals(inputPasswordConfirm)) {
				validationMessage += "入力されているパスワードと確認用パスワードが違います<br>";
				}

			//ログインIDに使用できない文字が含まれていないかをチェック
			if (!WebHelper.isLoginIdValidation(udb.getLoginId())) {
				validationMessage += "半角英数とハイフン、アンダースコアのみ入力できます<br>";
			}

			//メールアドレスが正しい形式かどうかチェック
			if(!WebHelper.isMailAdressValidation(udb.getMailAddress())) {
				validationMessage += "入力されたメールアドレスの形式が間違っています<br>";
			}

			//ログインIDの重複チェック
			if(UserDAO.isOverLapLoginId(udb.getLoginId(), 0)) {
				validationMessage += "ほかのユーザーが使用中のログインIDです";
			}

			//メールアドレスの重複チェック
			if(UserDAO.isOverLapMailAddress(udb.getMailAddress(), 0)) {
				validationMessage += "このメールアドレスはすでに登録されています";
			}

			//エラーがなければ確認画面へ遷移する
			if (validationMessage.length() == 0) {
				request.setAttribute("udb", udb);
				request.getRequestDispatcher(WebHelper.REGIST_CONFIRM_PAGE).forward(request, response);
			} else {
				session.setAttribute("udb", udb);
				session.setAttribute("validationMessage", validationMessage);
				response.sendRedirect("Regist");
			}
		}catch(Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}

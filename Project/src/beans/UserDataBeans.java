package beans;

import java.io.Serializable;

public class UserDataBeans implements Serializable {
	private String name;
	private String mailAddress;
	private String loginId;
	private String password;
	private int id;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMailAddress() {
		return mailAddress;
	}

	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	//ユーザー情報更新時にまとめてBeansにセットする
	public void setUpdateUserDataBeansInfo(String name, String loginId, String mailAddress, int id) {
		this.name = name;
		this.loginId = loginId;
		this.mailAddress = mailAddress;
		this.id = id;

	}

}



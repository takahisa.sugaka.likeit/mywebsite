package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.ReserveDataBeans;

public class ReserveDAO {

	public static void insertReserve(ReserveDataBeans rdb, String reserveDate) throws SQLException{
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO reserve (restaurant_id, user_id, num_people, reserve_date, reserve_time, is_cancel, create_date) "
					+ " values (?, ?, ?, ?, ?, false, now())"
					);
			st.setInt(1, rdb.getRestaurantId());
			st.setInt(2, rdb.getUserId());
			st.setInt(3, rdb.getnumPeople());
			st.setString(4, reserveDate);
			st.setString(5, rdb.getReserveTime());

			st.executeUpdate();
			System.out.println("Reservation completed");
		}catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally{
			if (con != null) {
				con.close();
			}
		}
	}

	public static List<Integer> getRecentReserveByUserId(int userId)throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		List<Integer> reserveRestaurantId = new ArrayList<Integer>();
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"SELECT DISTINCT restaurant_id, id " +
					"FROM reserve " +
					"WHERE user_id = ? " +
					"and is_cancel = false " +
					"ORDER BY id DESC " +
					"LIMIT 10");
			st.setInt(1, userId);

			ResultSet rs = st.executeQuery();

			while(rs.next()) {
				int getRestaurantId = rs.getInt("restaurant_id");
				Integer parseRestaurantId = Integer.valueOf(getRestaurantId);
				reserveRestaurantId.add(parseRestaurantId);
			}
		}catch(SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if (con != null) {
				con.close();
			}
		}
		return reserveRestaurantId;
	}

	public static ArrayList<ReserveDataBeans> getReserveListByUserId(int userId)throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		ArrayList<ReserveDataBeans> reserveList = new ArrayList<ReserveDataBeans>();
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"SELECT *" +
					"FROM reserve re " +
					"INNER JOIN " +
					"restaurant rest " +
					"ON " +
					"re.restaurant_id = rest.id " +
					"WHERE user_id = ? " +
					"and " +
					"reserve_date >= CURDATE()" +
					"and " +
					"is_cancel = FALSE");
			st.setInt(1, userId);

			ResultSet rs = st.executeQuery();

			while(rs.next()) {
				ReserveDataBeans reserve = new ReserveDataBeans();
				reserve.setId(rs.getInt("id"));
				reserve.setReserveDate(rs.getDate("reserve_date"));
				reserve.setReserveTime(rs.getString("reserve_time"));
				reserve.setRestaurantName(rs.getString("name"));
				reserve.setFileName(rs.getString("file_name"));
				reserveList.add(reserve);
			}

			return reserveList;
			}catch(SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static ArrayList<ReserveDataBeans> getPastReserveListByUserId(int userId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		ArrayList<ReserveDataBeans> pastReserveList = new ArrayList<ReserveDataBeans>();
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * " +
					"FROM reserve re " +
					"INNER JOIN " +
					"restaurant rest " +
					"ON " +
					"re.restaurant_id = rest.id " +
					"WHERE " +
					"user_id = ? " +
					"and " +
					"reserve_date < CURDATE() " +
					"or " +
					"user_id = ? " +
					"and " +
					"is_cancel = TRUE");
			st.setInt(1, userId);
			st.setInt(2, userId);

			ResultSet rs = st.executeQuery();
			while(rs.next()) {
				ReserveDataBeans reserve = new ReserveDataBeans();
				reserve.setId(rs.getInt("id"));
				reserve.setReserveDate(rs.getDate("reserve_date"));
				reserve.setReserveTime(rs.getString("reserve_time"));
				reserve.setRestaurantName(rs.getString("name"));
				reserve.setFileName(rs.getString("file_name"));
				reserve.setCancel(rs.getBoolean("is_cancel"));
				pastReserveList.add(reserve);
			}

			return pastReserveList;
		}catch(SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if (con != null) {
				con.close();
			}
		}

	}

	public static ReserveDataBeans getReserveDetail(int userId, int reserveId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT " +
					"rest.name, rest.address, re.id, re.reserve_date, re.reserve_time, re.num_people, re.is_cancel " +
					"FROM reserve re " +
					"INNER JOIN " +
					"restaurant rest " +
					"ON " +
					"re.restaurant_id = rest.id " +
					"WHERE " +
					"user_id = ? and re.id = ?");
			st.setInt(1, userId);
			st.setInt(2, reserveId);

			ResultSet rs = st.executeQuery();

			ReserveDataBeans rdb = new ReserveDataBeans();
			if(rs.next()) {
				rdb.setRestaurantName(rs.getString("name"));
				rdb.setRestaurantAddress(rs.getString("address"));
				rdb.setId(rs.getInt("id"));
				rdb.setReserveDate(rs.getDate("reserve_date"));
				rdb.setReserveTime(rs.getString("reserve_time"));
				rdb.setnumPeople(rs.getInt("num_people"));
				rdb.setCancel(rs.getBoolean("is_cancel"));
			}
			return rdb;
		}catch(SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static void reserveCancel(int userId, int reserveId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("UPDATE reserve SET is_cancel = true WHERE user_id =? and id =?");
			st.setInt(1, userId);
			st.setInt(2, reserveId);

			st.executeUpdate();

		}catch(SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if (con != null) {
				con.close();
			}
		}

	}
}

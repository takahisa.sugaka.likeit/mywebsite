package mywebsite;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.AreaDataBeans;
import beans.RestaurantDataBeans;
import dao.AreaDAO;
import dao.RestaurantDAO;

/**
 * Servlet implementation class RestaurantUpdateConfirm
 */
@WebServlet("/RestaurantUpdate")
public class RestaurantUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RestaurantUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		// マルチバイト文字列の文字化け対応
		request.setCharacterEncoding("UTF-8");
		try {
			//セッションからユーザーidを取得
			int userId = (int)session.getAttribute("userId");

			//管理者アカウントでログインしていなかったらリダイレクト
			if(userId != WebHelper.ADMIN_ID) {
				response.sendRedirect("Index");
				return;
			}
			String restaurantIdS =  session.getAttribute("restaurantIdS")!= null ? (String) WebHelper.cutSessionAttribute(session,"restaurantIdS"): request.getParameter("id");
			int restaurantId = Integer.parseInt(restaurantIdS);
			//レストラン情報をセッション領域から取得。なければidから取得
			RestaurantDataBeans restdb = session.getAttribute("restdbU")!= null ? (RestaurantDataBeans)WebHelper.cutSessionAttribute(session, "restdbU") :RestaurantDAO.getRestaurantById(restaurantId);

			//レストラン所在地を除いたエリア情報を取得
			ArrayList<AreaDataBeans> areaList = AreaDAO.getAreaListExclusionRestaurantLocation(restdb.getAreaId());
			//リクエスト領域とセッション領域に必要な値を設定
			session.setAttribute("restdbU", restdb);

			//キャンセルで戻ってきた時用
			request.setAttribute("areaList", areaList);


			request.getRequestDispatcher(WebHelper.RESTAURANT_UPDATE).forward(request, response);
		}catch(Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			}
		}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

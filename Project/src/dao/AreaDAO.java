package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.AreaDataBeans;

public class AreaDAO {

	public static int getAreaId(String area) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM area where name LIKE ?");
			st.setString(1, "%" + area + "%");
			ResultSet rs = st.executeQuery();
			int areaId = -1;
			while(rs.next()) {
				//検索結果にareaが含まれているかを確認する。
				if(rs.getString("name").contains(area))	{
					areaId = rs.getInt("id");
					System.out.println("Searching area found !");
					break;
				}
			}
			System.out.println("not found searching area!");
			return areaId;
		}catch(SQLException e){
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static String getAreaNameById(int areaId)throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM area where id = ?");
			st.setInt(1, areaId);
			ResultSet rs = st.executeQuery();

			String areaName = "";
			while(rs.next()) {
				areaName = rs.getString("name");
				break;
			}
			return areaName;
		}catch(SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static ArrayList<AreaDataBeans> getAreaList() throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		ArrayList<AreaDataBeans> areaList = new ArrayList<AreaDataBeans>();
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM area");

			ResultSet rs = st.executeQuery();
			while(rs.next()) {
				AreaDataBeans area = new AreaDataBeans();
				area.setId(rs.getInt("id"));
				area.setName(rs.getString("name"));
				areaList.add(area);
			}
			return areaList;
		}catch(SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if (con != null) {
				con.close();
			}
		}
	}



	public static ArrayList<AreaDataBeans> getAreaListExclusionRestaurantLocation(int restaurantId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;ArrayList<AreaDataBeans> areaList = new ArrayList<AreaDataBeans>();
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM area where id NOT IN(?)");
			st.setInt(1, restaurantId);

			ResultSet rs = st.executeQuery();
			while(rs.next()) {
				AreaDataBeans area = new AreaDataBeans();
				area.setId(rs.getInt("id"));
				area.setName(rs.getString("name"));
				areaList.add(area);
			}
			return areaList;
		}catch(SQLException e){
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if (con != null) {
				con.close();
			}
		}
	}


}

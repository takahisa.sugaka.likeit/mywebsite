<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>レストラン情報詳細</title>
        <link rel="stylesheet"
              href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">
        <link href="css/admin_style.css" rel="stylesheet" type="text/css" />
    </head>

    <body>
        <jsp:include page="/baselayout/header.jsp" />

        <div class="container">
            <div class="row mt-4">
                <h4 class="col-md-12 text-center reservelist-header">レストラン情報詳細</h4>
            </div>
            <div class="row">
                <div class="col-md-12 reserve-result">
                    <table class="table table-bordered">
                        <tr>
                            <th>レストラン名</th>
                            <td><h5>${rdb.name}</h5></td>
                        </tr>
                        <tr>
                            <th>エリア</th>
                            <td><p>${rdb.areaName}</p></td>
                        </tr>
                        <tr>
                            <th>電話番号</th>
                            <td><p>${rdb.phoneNumber}</p></td>
                        </tr>
                        <tr>
                            <th>住所</th>
                            <td><p>${rdb.address}</p></td>
                        </tr>
                        <tr>
                            <th>レストランPR</th>
                            <td><p>${rdb.pr}</p></td>
                        </tr>
                        <tr>
                            <th>レストラン詳細</th>
                            <td><p>${rdb.detail}</p></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row mx-auto">
				<div class="col-offset-5 col-md-2 mx-auto" >
                	<a href="Admin">戻る</a>
                </div>
            </div>
        </div>
    </body>
</html>
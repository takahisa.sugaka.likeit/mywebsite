package mywebsite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.RestaurantDAO;

/**
 * Servlet implementation class restaurantDeleteResult
 */
@WebServlet("/RestaurantDeleteResult")
public class RestaurantDeleteResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RestaurantDeleteResult() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			//セッションからユーザーidを取得
			int userId = (int)session.getAttribute("userId");

			//管理者アカウントでログインしていなかったらリダイレクト
			if(userId != WebHelper.ADMIN_ID) {
				response.sendRedirect("Index");
				return;
			}

			//押されたボタンを確認する
			String confirmed = request.getParameter("confirmButton");
			//押されたボタンによって処理が分岐
			switch(confirmed) {
			//戻るボタンが押されていたら管理画面へ戻る
			case "cancel":
				session.removeAttribute("restaurantId");
				response.sendRedirect("Admin");
				break;
			//削除ボタンが押されていたら、レストラン情報を削除して完了画面へ遷移
			case "delete":
				//セッション領域からレストランIDを取得および削除
				int restaurantId = (int)session.getAttribute("restaurantId");
				session.removeAttribute("restaurantId");
				//削除を実行
				RestaurantDAO.restaurantDelete(restaurantId);
				request.getRequestDispatcher(WebHelper.RESTAURANT_DELETE_RESULT).forward(request, response);
			}

		}catch(Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");

		}
	}

}

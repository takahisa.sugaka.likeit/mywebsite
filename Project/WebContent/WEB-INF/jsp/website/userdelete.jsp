<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>ユーザー削除確認</title>
        <link rel="stylesheet"
              href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">
        <link href="css/style.css" rel="stylesheet" type="text/css" />
    </head>

    <body>
        <jsp:include page="/baselayout/header.jsp" />

        <div class="container">
            <div class="row">
                <div class="withdraw-confirm-message mx-auto mt-5">
                    <h5>下記ボタンを押すとユーザー情報は全て削除されますがよろしいですか？</h5>
                </div>
            </div>
            <form action="UserDeleteResult" method="post">
            <div class="row mt-5">
                <div class="col-md-12 text-center">
                    <button class="col-md-3 btn btn-danger" type="submit" name="confirmButton" value="delete">はい、退会します</button>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-md-12 text-center">
                    <button class="col-md-3 btn btn-secondary" type="submit" name="confirmButton" value="cancel">いいえ、退会しません</button>
                </div>
            </div>
            </form>
        </div>

    </body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>レストラン情報更新</title>
        <link rel="stylesheet"
              href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">
        <link href="css/admin_style.css" rel="stylesheet" type="text/css" />
    </head>

    <body>
        <jsp:include page="/baselayout/admin_header.jsp" />

        <div class="container">
            <div class="row mt-4">
                <h4 class="col-md-12 text-center reservelist-header">レストラン情報更新</h4>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card reserve-confirm col-md-12">
                        <form method="POST"  enctype="multipart/form-data" action="RestaurantUpdateConfirm">
                            <div class="card-content">
                                <div class="row">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>レストラン名</th>
                                            <td><input type="text" value="${restdbU.name}" name="restaurant_name" class="form-control"></td>
                                        </tr>
                                        <tr>
                                            <th>エリア</th>
                                            <td>
                                                <select name="area_id" class="form-control">
                                                	<option value="${restdbU.areaId}" >${restdbU.areaName}</option>
                                                	<c:forEach var="area" items="${areaList}">
	                                            		<option value="${area.id}"  >${area.name}</option>
	                                            	</c:forEach>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>電話番号</th>
                                            <td><input type="tel" value="${restdbU.phoneNumber}" name="phone_number" class="form-control"></td>
                                        </tr>
                                        <tr>
                                            <th>住所</th>
                                            <td><input type="text" value="${restdbU.address}" name="address" class="form-control"></td>
                                        </tr>
                                        <tr>
                                            <th>写真</th>
                                            <td><input type="file" name ="photo" class="form-control"></td>
                                        </tr>
                                        <tr>
                                            <th>レストランPR</th>
                                            <td><textarea cols="112" rows="2" name="pr" class="form-control">${restdbU.pr}</textarea></td>
                                        </tr>
                                        <tr>
                                            <th>レストラン詳細</th>
                                            <td><textarea cols="112" rows="6" name="detail" class="form-control">${restdbU.detail}</textarea></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12 text-center">
                            	<input type="hidden" value="${restdbU.id}" name="id">
                                <input type="submit" value="更新">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
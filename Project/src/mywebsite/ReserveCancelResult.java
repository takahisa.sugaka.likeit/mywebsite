package mywebsite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ReserveDAO;

/**
 * Servlet implementation class ReserveCancelResult
 */
@WebServlet("/ReserveCancelResult")
public class ReserveCancelResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReserveCancelResult() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			//セッションからユーザーidを取得
			int userId = (int) session.getAttribute("userId");
			//予約idを取得
			int reserveId = Integer.parseInt(request.getParameter("reserve_id"));

			//押されたボタンの値を取得
			String confirmed = request.getParameter("confirmButton");

			switch(confirmed) {
			//戻るボタンが押されていた場合
			case "return":
				session.setAttribute("reserveId", reserveId);
				response.sendRedirect("ReserveList");
				break;
			//キャンセルボタンが押されていた場合
			case "cancel":
				ReserveDAO.reserveCancel(userId,reserveId);
				request.getRequestDispatcher(WebHelper.CANCEL_CONFIRM_RESULT_PAGE).forward(request, response);
				break;
			}
		}catch(Exception e){
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

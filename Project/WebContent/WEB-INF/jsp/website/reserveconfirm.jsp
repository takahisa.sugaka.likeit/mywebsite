<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
	<head>
	    <meta charset="UTF-8">
	    <title>予約確認</title>
	    <link rel="stylesheet"
	          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	          crossorigin="anonymous">
	    <link href="css/style.css" rel="stylesheet" type="text/css" />
	</head>

	 <body>
	 	<jsp:include page="/baselayout/header.jsp" />

	    <div class="container">
			<div class="row">
				<h5 class="col-md-12 text-center mt-3 mb-5">以下の内容で予約しますか？</h5>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="card reserve-confirm col-md-12">
						<div class="card-content">
							<form method="POST" action="ReserveResult">
								<div class="row">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th class="text-center" style="width: 30%" >来店日</th>
												<th class="text-center">時間</th>
												<th class="text-center" style="width: 30%" >人数</th>
											</tr>
										</thead>
										<tbody>
									        <tr>
	                                            <td class="text-center">${reserveDate}</td>
	                                            <td class="text-center">${reserveTime}</td>
	                                            <td class="text-center">${numPeople}人</td>
									        </tr>
										</tbody>
									</table>
								</div>
								<div class="row text-center">
									<div class="col s12">
										<button class="btn btn-reserve btn-orange" type="submit" name="reserve">予約</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

	</body>
</html>
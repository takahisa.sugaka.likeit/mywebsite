package mywebsite;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.RestaurantDataBeans;
import dao.AreaDAO;
import dao.RestaurantDAO;



/**
 * Servlet implementation class Admin
 */
@WebServlet("/Admin")
public class Admin extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Admin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			//セッションからユーザーidを取得
			int userId = (int)session.getAttribute("userId");

			//管理者アカウントでログインしていなかったらリダイレクト
			if(userId != WebHelper.ADMIN_ID) {
				response.sendRedirect("Index");
				return;
			}

			//レストランを格納するリストを定義
			ArrayList<RestaurantDataBeans> restaurantSearchResult = null;
			//総ページ数取得のための変数
			double restCount = 0;
			int pageMax = 0;

			//セッション領域のレストランBeansを削除
			session.removeAttribute("restdb");
			session.removeAttribute("restdbU");
			//表示ページ番号 未指定の場合 1ページ目を表示
			int pageNum = Integer.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));

			//入力された文字を変数に代入する
			String area = request.getParameter("area");
			String name = request.getParameter("restaurant_name");
			String phoneNum = request.getParameter("phone_number");
			String address = request.getParameter("address");

			request.setAttribute("area", area);
			request.setAttribute("name", name);
			request.setAttribute("phoneNum", phoneNum);
			request.setAttribute("address", address);

			int areaId = 0;

			//初めてこのページを読み込んだ時の処理
			if(areaId == 0 && name == null && phoneNum == null && address == null) {
				//レストランを全検索する
				restaurantSearchResult = RestaurantDAO.searchAll(pageNum, WebHelper.PAGE_MAX_ITEM_COUNT);
				restCount = RestaurantDAO.getRestaurantCountAll();
			}else if(areaId == 0 && name.equals("") && phoneNum.equals("") && address.equals("")) {
				//レストランを全検索する
				restaurantSearchResult = RestaurantDAO.searchAll(pageNum, WebHelper.PAGE_MAX_ITEM_COUNT);
				restCount = RestaurantDAO.getRestaurantCountAll();
			}

			//エリアが入力されているかを確認
			if(area != null && !area.equals("")) {
				areaId = AreaDAO.getAreaId(area);

			}

			//エラーメッセージを格納する変数
			String validationMessage = "";
			//地域が入力されている時
			if(!(areaId == 0) && !(areaId == -1)) {
				restaurantSearchResult = RestaurantDAO.getRestaurant(areaId, name, phoneNum, address, pageNum, WebHelper.PAGE_MAX_ITEM_COUNT);
				restCount = RestaurantDAO.getRestaurantCount(name, areaId, phoneNum, address);
			}
			if(area != null && areaId == 0) {
				restaurantSearchResult = RestaurantDAO.getRestaurant(areaId, name, phoneNum, address, pageNum, WebHelper.PAGE_MAX_ITEM_COUNT);
				restCount = RestaurantDAO.getRestaurantCount(name, areaId, phoneNum, address);
			}
			if(areaId == -1) {
				//エラーメッセージを格納する変数
				validationMessage = "その地域は登録されていません";
			}

			//検索ワードに対する総ページ数を取得
			pageMax = (int) Math.ceil(restCount/WebHelper.PAGE_MAX_ITEM_COUNT);

			//総アイテム数
			request.setAttribute("itemCount", (int) restCount);
			// 総ページ数
			request.setAttribute("pageMax", pageMax);
			// 表示ページ
			request.setAttribute("pageNum", pageNum);
			request.setAttribute("restaurantList", restaurantSearchResult);

			request.setAttribute("validationMessage", validationMessage);

			request.getRequestDispatcher(WebHelper.ADMIN_INDEX).forward(request, response);
		}catch(Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}


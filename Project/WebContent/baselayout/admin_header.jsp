<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<header>
 	<nav class="navbar navbar-light mb-2">
    	<a href="Admin" class="navbar-brand">TOP</a>
        <ul class="nav justify-content-end">
			<% boolean isLogin = session.getAttribute("isLogin") !=null?(boolean)session.getAttribute("isLogin"):false; %>

            <% if(isLogin){ %>
            <li class="nav-item"><a class="nav-link active" href="Logout">ログアウト</a></li>
            <% }else{ %>
            <li class="nav-item"><a class="nav-link active" href="Login">ログイン</a></li>
            <%} %>
         </ul>
     </nav>
</header>
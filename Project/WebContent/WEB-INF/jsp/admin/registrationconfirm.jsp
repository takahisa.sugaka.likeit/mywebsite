<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>新規登録確認</title>
        <link rel="stylesheet"
              href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">
        <link href="css/style.css" rel="stylesheet" type="text/css" />
    </head>

    <body>
       <jsp:include page="/baselayout/admin_header.jsp" />

        <div class="container">
            <div class="row mt-4">
                <h4 class="col-md-12 text-center reservelist-header">レストラン新規登録確認</h4>
            </div>
	        <div class="row">
	            <div class="col-md-12 reserve-result">
	                <table class="table table-bordered">
			            <tr>
					        <th>レストラン名</th>
					        <td>${restdbS.name}</td>
			            </tr>
			            <tr>
					        <th>エリア</th>
					        <td>${restdbS.areaName}</td>
			            </tr>
			            <tr>
					        <th>電話番号</th>
					        <td>${restdbS.phoneNumber}</td>
			            </tr>
			            <tr>
					        <th>住所</th>
					        <td>${restdbS.address}</td>
						</tr>
						<tr>
							<th>写真</th>
		 					<td><img src="tmp/${restdbS.fileName}" class="card-img" alt="..."></td>
	 					</tr>
						<tr>
							<th>レストランPR</th>
							<td>${restdbS.pr}</td>
						</tr>
						<tr>
							<th>レストラン詳細</th>
							<td>${restdbS.detail}</td>
						</tr>
	                </table>
	            </div>
	        </div>
				<form method="POST" action="RestaurantRegistrationResult">
	            <div class="row mt-5">
		           	<div class="col-md-6 col-sm-6">
						<button class="btn btn-primary col-md-4 offset-md-4" type="submit" name="confirmButton" value="cancel">戻る</button>
					</div>
		            <div class="col-md-6 col-sm-6">
		                <button class="btn btn-danger col-md-4 offset-md-4" type="submit" name="confirmButton" value="update">登録する</button>
					</div>
	            </div>
            </form>

        </div>
    </body>
</html>
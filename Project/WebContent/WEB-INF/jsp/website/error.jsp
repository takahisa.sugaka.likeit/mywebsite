<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>システムエラー</title>
        <link rel="stylesheet"
              href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">
        <link href="css/style.css" rel="stylesheet" type="text/css" />
    </head>

    <body>
    	<jsp:include page="/baselayout/header.jsp" />


        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="card reserve-confirm offset-2 col-md-8">
                        <div class="card-content">
                            <div class="row">
                                <div class="mx-auto">
                                    <h4 class="justify-content-center mt-2 text-danger">
                                        システムエラーが発生しました
                                    </h4>
                                </div>
                            </div>
                            <div class="row text-center">

                                <div class="col-md-12">
                                    <a class="btn btn-reserve btn-success" href="Index">TOPへ戻る</a>
                                </div>
                             </div>
                        </div>
				    </div>
                </div>
            </div>
        </div>
    </body>
</html>
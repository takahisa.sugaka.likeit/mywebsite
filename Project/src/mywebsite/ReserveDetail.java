package mywebsite;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ReserveDataBeans;
import dao.ReserveDAO;

/**
 * Servlet implementation class ReserveDetail
 */
@WebServlet("/ReserveDetail")
public class ReserveDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReserveDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			//セッションからuserIdを取得
			int userId = (int) session.getAttribute("userId");
			//予約IDを取得
			int reserveId = session.getAttribute("reserveId") !=null ?(int)WebHelper.cutSessionAttribute(session, "reserveId") : Integer.parseInt(request.getParameter("reserve_id"));
			//ユーザーidと予約idでSELECT文を実行
			ReserveDataBeans rdb = ReserveDAO.getReserveDetail(userId, reserveId);

			//今日の日付を取得
			Date today = new Date();
			//jspでの表示切替用変数
			Boolean dateComparison = false;
			//今日の日付と予約した日付を比較
			if(rdb.getReserveDate().after(today)) {
				dateComparison = true;
			}
			request.setAttribute("dateComparison", dateComparison);
			request.setAttribute("rdb", rdb);
			request.getRequestDispatcher(WebHelper.RESERVE_DETAIL_PAGE).forward(request, response);
		}catch(Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

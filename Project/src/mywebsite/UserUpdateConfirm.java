package mywebsite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class UserUpdateConfirm
 */
@WebServlet("/UserUpdateConfirm")
public class UserUpdateConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateConfirm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			//エラーメッセージを格納する変数
			String validationMessage = "";

			UserDataBeans udb = new UserDataBeans();
			udb.setUpdateUserDataBeansInfo(request.getParameter("user_name"), request.getParameter("login_id"), request.getParameter("user_mail_address"), (int)session.getAttribute("userId"));

			System.out.println(udb.getMailAddress());
			//ログインIDに使用できない文字が含まれていないかをチェック
			if (!WebHelper.isLoginIdValidation(udb.getLoginId())) {
				validationMessage += "半角英数とハイフン、アンダースコアのみ入力できます<br>";
			}

			//ログインIDの重複チェック
			if(UserDAO.isOverLapLoginId(udb.getLoginId(), (int) session.getAttribute("userId"))) {
				validationMessage += "ほかのユーザーが使用中のログインIDです<br>";
			}

			//メールアドレスが正しい形式かどうかチェック
			if(!WebHelper.isMailAdressValidation(udb.getMailAddress())) {
				validationMessage += "入力されたメールアドレスの形式が間違っています<br>";
			}

			//メールアドレスの重複チェック
			if(UserDAO.isOverLapMailAddress(udb.getMailAddress(), (int) session.getAttribute("userId"))) {
				validationMessage += "このメールアドレスはすでに登録されています<br>";
			}

			//ValidationMessageにメッセージがなかったら確認画面へ遷移
			if(validationMessage.length() == 0) {
				request.setAttribute("udb", udb);
				request.getRequestDispatcher(WebHelper.USER_UPDATE_CONFIRM_PAGE).forward(request, response);
			}else {
				//セッションにエラーメッセージを格納してマイページへ
				session.setAttribute("validationMessage", validationMessage);
				response.sendRedirect("UserPage");

			}
			}catch(Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
	<head>
	    <meta charset="UTF-8">
	    <title>検索結果</title>
	    <link rel="stylesheet"
	          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	          crossorigin="anonymous">
	    <link href="css/style.css" rel="stylesheet" type="text/css" />
	</head>

	<body>
	    <jsp:include page="/baselayout/header.jsp" />

		<div class="container">
	    	<div class="row">
		    	<div class="mx-auto">
		        	<div class="search">
			            <form class="search-form form-inline" method="get" action="SearchResult">
				            <input type="text" placeholder="エリア・駅" name="area" />
				            <input type="text" placeholder="キーワード" name="keyword" />
				            <button>検索</button>
			            </form>
		            </div>
		            <div class="row mx-auto">
					    <c:if test="${areaId== -1}">
					    	<p>${message}</p>
					    </c:if>
				    </div>

			        <c:forEach var="rest" items="${restaurantList}">
				        <div class="restaurant-module">
						    <div class="card mb-3" style="max-width: 650px;">
							    <div class="row no-gutters">
								    <div class="col-md-4">
								    	<img src="img/${rest.fileName}" class="card-img" alt="...">
								    </div>
								    <div class="col-md-8">
									    <div class="card-body">
										    <h5 class="restaurant-name">${rest.name}</h5>
										    <p class="restaurant-description">${rest.pr}</p>
										    <p class="restaurant-area">住所：${rest.address}</p>
										    <p class="restaurant-tel">TEL：${rest.phoneNumber}</p>
										    <a class="btn btn-restaurant-detail btn-orange" href="Restaurant?restaurant_id=${rest.id}&page_num=${pageNum}">店舗詳細・予約</a>
									    </div>
								    </div>
							    </div>
						    </div>
					    </div>
				    </c:forEach>
				</div>
			</div>
			<div class="row">
				<div class="col-offset-5 col-md-2 mx-auto" >
					<nav aria-label="Page navigation example">
						<ul class="pagination">

						<!-- 1ページ戻るボタン -->
						<c:choose>
							<c:when test="${pageNum == 1}">
							    <li class="page-item disabled">
									<a class="page-link" href="" aria-label="Previous">
									    <span aria-hidden="true">&laquo;</span>
									    <span class="sr-only">Previous</span>
									</a>
							    </li>
							</c:when>
						    <c:otherwise>
							    <li class="page-item">
									<a class="page-link" href="SearchResult?area=${area}&keyword=${keyWord}&page_num=${pageNum - 1}" aria-label="Previous">
									    <span aria-hidden="true">&laquo;</span>
										<span class="sr-only">Previous</span>
									</a>
								</li>
							</c:otherwise>
						</c:choose>

						<!-- ページインデックス -->
						<c:forEach begin="${(pageNum - 5) > 0 ? pageNum - 5 : 1}" end="${(pageNum + 5) > pageMax ? pageMax : pageNum + 5}" step="1" varStatus="status">
							<li class="page-item" <c:if test="${pageNum == status.index }"> class="active" </c:if>><a class="page-link" href="SearchResult?area=${area}&keyword=${keyword}&page_num=${status.index}">${status.index}</a></li>
						</c:forEach>

						<!-- 1ページ進むボタン -->
						<c:choose>
							<c:when test="${pageNum == pageMax || pageMax == 0}">
								<li class="page-item disabled">
									<a class="page-link" href="#" aria-label="Next">
										<span aria-hidden="true">&raquo;</span>
										<span class="sr-only">Next</span>
								    </a>
							    </li>
							</c:when>
						    <c:otherwise>
							    <li class="page-item">
								    <a class="page-link" href="SearchResult?area=${area}&keyword=${keyWord}&page_num=${pageNum + 1}" aria-label="Next">
									    <span aria-hidden="true">&raquo;</span>
										<span class="sr-only">Next</span>
								    </a>
								</li>
							</c:otherwise>
					    </c:choose>
						</ul>
					</nav>
			    </div>
			</div>
		</div>
	</body>
</html>
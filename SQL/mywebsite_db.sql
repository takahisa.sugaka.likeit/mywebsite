CREATE DATABASE Mywebsite DEFAULT CHARSET utf8;

USE Mywebsite;

CREATE TABLE area(
	id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	name varchar(256) NOT NULL
	);
	
CREATE TABLE reserve(
	id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	num_reserve int NOT NULL,
	reserve_date date NOT NULL,
	is_cancel boolean NOT NULL,
	create_date date NOT NULL,
	restaurant_id int NOT NULL,
	user_id int NOT NULL
);

CREATE TABLE restaurant(
	id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	name varchar(256),
	phone_number varchar(20) NOT NULL UNIQUE,
	address varchar(256) NOT NULL,
	file_name varchar(256),
	area_id int NOT NULL
);

CREATE TABLE user(
	id int NOT NULL PRIMARY KEY,
	name varchar(256) NOT NULL,
	mail_address varchar(256) NOT NULL UNIQUE,
	login_id varchar(256) NOT NULL,
	login_password varchar(256) NOT NULL,
	create_date date NOT NULL
);
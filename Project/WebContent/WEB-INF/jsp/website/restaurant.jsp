<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>レストラン詳細</title>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <jsp:include page="/baselayout/header.jsp" />

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-12">
                <div class="rst-name-area">
                <h2 class="restaurant-name">
                    ${rest.name}
                </h2>
            </div>
                <div class="main-content">
                    <div class="rst-img">
                        <img src="img/${rest.fileName}" class="card-img" alt="...">
                    </div>
                    <div class="detail-area mb-2 pb-2">
                        <h4 class="detail-header">店舗詳細</h4>
                        <div class="rst-detail">
                            <P>${rest.detail}</P>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="side-nav">
                    <div class="reserve">
                        <div class="reserve-comment">
                            <h5>予約はこちらから</h5>
                        </div>
                        <div class="form-group">
                            <form class="reserveForm" id="reserveForm" method="post" action="ReserveConfirm">
                            	<input name="rest_id" type="hidden" value="${rest.id}">
                                <p>来店日</p>
                                <input type="date" name="reserve_date" class="form-control" required>

                                <p>時間</p>
                                <select form="reserveForm" name="reserve_time" class="form-control">
                                    <option value="19:00">19:00</option>
                                    <option value="20:00">20:00</option>
                                    <option value="21:00">21:00</option>
                                    <option value="22:00">22:00</option>
                                    <option value="23:00">23:00</option>
                                </select>
                                <p>人数</p>
                                <select form="reserveForm" name="num_people" class="form-control">
                                    <option value="1">1人</option>
                                    <option value="2">2人</option>
                                    <option value="3">3人</option>
                                    <option value="4">4人</option>
                                    <option value="5">5人</option>
                                    <option value="6">6人</option>
                                </select>
                                <p><button class="btn btn-reserve btn-orange" type="submit">予約する</button></p>
                            </form>
                       </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</body>
</html>
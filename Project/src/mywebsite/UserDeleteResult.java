package mywebsite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDAO;

/**
 * Servlet implementation class UserDeleteResult
 */
@WebServlet("/UserDeleteResult")
public class UserDeleteResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteResult() {
        super();
        // TODO Auto-generated constructor stub
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 HttpSession session = request.getSession();
		 try {
			 //押されたボタンを確認
			 String confirmed = request.getParameter("confirmButton");
			 //押されたボタンによって処理が分岐
			 switch(confirmed) {
			 //退会しますが押されていた場合
			 case "delete":
				 //ユーザーidを取得
				 int userId = (int) session.getAttribute("userId");
				 //ユーザーidをもとにDELETE文を実行
				 UserDAO.userDelete(userId);
				 //セッションからユーザー情報とログイン情報を削除
				 session.setAttribute("isLogin", false);
				 session.removeAttribute("userId");
				 //アカウント削除完了画面に遷移
				 request.getRequestDispatcher(WebHelper.USER_DELETE_RESULT_PAGE).forward(request, response);
				 break;
			 //退会しませんが押されていた場合
			 case "cancel":
				 response.sendRedirect("UserPage");
				 break;
			 }


		 }catch(Exception e) {
			 e.printStackTrace();
			 session.setAttribute("errorMessage", e.toString());
			 response.sendRedirect("Error");
		 }
	}
}

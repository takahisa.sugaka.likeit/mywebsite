package mywebsite;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ReserveDataBeans;

/**
 * Servlet implementation class ReserveConfirm
 */
@WebServlet("/ReserveConfirm")
public class ReserveConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReserveConfirm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* 文字化け対策 */
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			//予約用のデータが存在しているかを確認
			ReserveDataBeans rdb = (ReserveDataBeans) session.getAttribute("rdb") ;

			//ログインしているかどうかをチェック
			Boolean isLogin = session.getAttribute("isLogin") != null ? (Boolean) session.getAttribute("isLogin") : false;

			if(!isLogin) {
				// ログインしてないときに直接URLを入力したらINDEXにリダイレクトされる処理
				response.sendRedirect("Index");
			}else{
				// フォーマット変換用
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");

				//変換した日付を改めてinputReserveDateに代入
				String inputReserveDate = sdf.format(rdb.getReserveDate());
				//inputDataの値をリクエスト領域に代入
				request.setAttribute("reserveDate", inputReserveDate);
				request.setAttribute("reserveTime", rdb.getReserveTime());
				request.setAttribute("numPeople", rdb.getnumPeople());


				rdb.setUserId((int) session.getAttribute("userId"));
				session.setAttribute("rdb", rdb);

				request.getRequestDispatcher(WebHelper.RESERVE_CONFIRM).forward(request, response);
			}

		}catch(Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* 文字化け対策 */
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			//
			String restaurantId = request.getParameter("rest_id");
			String inputReserveDate = request.getParameter("reserve_date");

			//ReserveResultに遷移した時にDBに登録するようの日付をセッション領域にセット
			session.setAttribute("inputReserveDate", inputReserveDate);
			//予約用のデータが存在しているかを確認
			ReserveDataBeans rdb = (ReserveDataBeans) session.getAttribute("rdb") ;

			if(rdb == null) {
				// 「yyyy-MM-dd」の形式を「yyyy/MM/dd」で文字列置換する
				String strDate = inputReserveDate.replace("-", "/");
				// フォーマット変換用
				SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy/MM/dd");
				// Date型で取得する
				Date date = sdFormat.parse(strDate);

				rdb = new ReserveDataBeans();
				rdb.setRestaurantId(Integer.parseInt(restaurantId));
				rdb.setReserveDate(date);
				rdb.setReserveTime(request.getParameter("reserve_time"));
				rdb.setnumPeople(Integer.parseInt(request.getParameter("num_people")));

				session.setAttribute("rdb", rdb);

			}

			//ログインしているかどうかをチェック
			Boolean isLogin = session.getAttribute("isLogin") != null ? (Boolean) session.getAttribute("isLogin") : false;

			if(!isLogin) {
				//Sessionにリターンページの情報を格納
				session.setAttribute("returnStrUrl", "ReserveConfirm");
				// Login画面にリダイレクト
				response.sendRedirect("Login");
			}else{
				rdb.setUserId((int) session.getAttribute("userId"));
				session.setAttribute("rdb", rdb);
				// フォーマット変換用
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");

				//変換した日付を改めてinputReserveDateに代入
				inputReserveDate = sdf.format(rdb.getReserveDate());
				//inputDataの値をリクエスト領域に代入
				request.setAttribute("reserveDate", inputReserveDate);
				request.setAttribute("reserveTime", rdb.getReserveTime());
				request.setAttribute("numPeople", rdb.getnumPeople());

				request.getRequestDispatcher(WebHelper.RESERVE_CONFIRM).forward(request, response);
			}

		}catch(Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");

		}


	}

}

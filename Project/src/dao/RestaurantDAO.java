package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import base.DBManager;
import beans.RestaurantDataBeans;

public class RestaurantDAO {

	public static ArrayList<RestaurantDataBeans> getRandRest(int limit) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM restaurant ORDER BY RAND() LIMIT ? ");
			st.setInt(1, limit);

			ResultSet rs = st.executeQuery();

			ArrayList<RestaurantDataBeans> restaurantList = new ArrayList<RestaurantDataBeans>();

			while(rs.next()) {
				RestaurantDataBeans rest = new RestaurantDataBeans();
				rest.setId(rs.getInt("id"));
				rest.setName(rs.getString("name"));
				rest.setPhoneNumber(rs.getString("phone_number"));
				rest.setAddress(rs.getString("address"));
				rest.setDetail(rs.getString("detail"));
				rest.setPr(rs.getString("pr"));
				rest.setFileName(rs.getString("file_name"));
				rest.setAreaId(rs.getInt("area_id"));
				restaurantList.add(rest);
			}

			return restaurantList;

		}catch(SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//レストラン全件検索
	public static ArrayList<RestaurantDataBeans> searchAll(int pageNum, int pageMaxItemCount)throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			int startiItemNum = (pageNum - 1) * pageMaxItemCount;
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM restaurant ORDER BY area_id ASC LIMIT ?,?");
			st.setInt(1, startiItemNum);
			st.setInt(2, pageMaxItemCount);
			ResultSet rs = st.executeQuery();
			ArrayList<RestaurantDataBeans> restaurantList = new ArrayList<RestaurantDataBeans>();

			while(rs.next()) {
				RestaurantDataBeans rest = new RestaurantDataBeans();
				rest.setId(rs.getInt("id"));
				rest.setName(rs.getString("name"));
				rest.setPhoneNumber(rs.getString("phone_number"));
				rest.setAddress(rs.getString("address"));
				rest.setDetail(rs.getString("detail"));
				rest.setPr(rs.getString("pr"));
				rest.setFileName(rs.getString("file_name"));
				rest.setAreaId(rs.getInt("area_id"));
				restaurantList.add(rest);
			}

			return restaurantList;

		}catch(SQLException e){
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if (con != null) {
				con.close();
			}
		}
	}
	//店舗名入力時にレストラン検索するメソッド（ユーザー用）
	public static ArrayList<RestaurantDataBeans> getRestaurantByName(String keyWord, int pageNum, int pageMaxItemCount) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			int startiItemNum = (pageNum - 1) * pageMaxItemCount;
			con = DBManager.getConnection();
			if(keyWord.length() == 0) {
				//全検索メソッド
				st = con.prepareStatement("SELECT * FROM restaurant ORDER BY area_id ASC LIMIT ?,?");
				st.setInt(1, startiItemNum);
				st.setInt(2, pageMaxItemCount);
			}else {
				//店舗名での検索
				st = con.prepareStatement("SELECT * FROM restaurant WHERE name LIKE ? ORDER BY area_id ASC LIMIT ?,? ");
				st.setString(1, "%" + keyWord + "%");
				st.setInt(2, startiItemNum);
				st.setInt(3, pageMaxItemCount);
			}

			ResultSet rs = st.executeQuery();
			ArrayList<RestaurantDataBeans> restaurantList = new ArrayList<RestaurantDataBeans>();

			while(rs.next()) {
				RestaurantDataBeans rest = new RestaurantDataBeans();
				rest.setId(rs.getInt("id"));
				rest.setName(rs.getString("name"));
				rest.setPhoneNumber(rs.getString("phone_number"));
				rest.setAddress(rs.getString("address"));
				rest.setDetail(rs.getString("detail"));
				rest.setPr(rs.getString("pr"));
				rest.setFileName(rs.getString("file_name"));
				rest.setAreaId(rs.getInt("area_id"));
				restaurantList.add(rest);
			}

			return restaurantList;
		}catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static ArrayList<RestaurantDataBeans> getRestaurantByNameAndId(String keyWord, int areaId, int pageNum, int pageMaxItemCount) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
				int startiItemNum = (pageNum - 1) * pageMaxItemCount;
				con = DBManager.getConnection();
				if(keyWord.length() == 0) {
					//エリアIDのみでの検索
					st = con.prepareStatement("SELECT * FROM restaurant WHERE area_id=? ORDER BY id ASC LIMIT ?,?");
					st.setInt(1, areaId);
					st.setInt(2, startiItemNum);
					st.setInt(3, pageMaxItemCount);
				}else {
					//店舗名での検索
					st = con.prepareStatement("SELECT * FROM restaurant WHERE name LIKE ? AND area_id = ? ORDER BY id ASC LIMIT ?,? ");
					st.setString(1, "%" + keyWord + "%");
					st.setInt(2, areaId);
					st.setInt(3, startiItemNum);
					st.setInt(4, pageMaxItemCount);
				}

				ResultSet rs = st.executeQuery();
				ArrayList<RestaurantDataBeans> restaurantList = new ArrayList<RestaurantDataBeans>();

				while(rs.next()) {
					RestaurantDataBeans rest = new RestaurantDataBeans();
					rest.setId(rs.getInt("id"));
					rest.setName(rs.getString("name"));
					rest.setPhoneNumber(rs.getString("phone_number"));
					rest.setAddress(rs.getString("address"));
					rest.setDetail(rs.getString("detail"));
					rest.setPr(rs.getString("pr"));
					rest.setFileName(rs.getString("file_name"));
					rest.setAreaId(rs.getInt("area_id"));
					restaurantList.add(rest);
				}

				return restaurantList;
			}catch(SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);
			}finally {
				if (con != null) {
					con.close();
				}
			}
		}



	//管理画面でフォームを全て埋めた時の検索メソッド
	public static ArrayList<RestaurantDataBeans> getRestaurantByfilledInForm(int areaId, String name, String phoneNum,
			String address, int pageNum, int pageMaxItemCount) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			int startiItemNum = (pageNum - 1) * pageMaxItemCount;
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM restaurant WHERE area_id = ? and name LIKE ? and"
					+ " phone_number LIKE ? and address LIKE ? ORDER BY id ASC LIMIT ?,?");
			st.setInt(1, areaId);
			st.setString(2, "%" + name + "%");
			st.setString(3, "%" + phoneNum + "%");
			st.setString(4, "%" + address + "%");
			st.setInt(5, startiItemNum);
			st.setInt(6, pageMaxItemCount);
			ResultSet rs = st.executeQuery();
			ArrayList<RestaurantDataBeans> restaurantList = new ArrayList<RestaurantDataBeans>();

			while(rs.next()) {
				RestaurantDataBeans rest = new RestaurantDataBeans();
				rest.setId(rs.getInt("id"));
				rest.setName(rs.getString("name"));
				rest.setPhoneNumber(rs.getString("phone_number"));
				rest.setAddress(rs.getString("address"));
				rest.setDetail(rs.getString("detail"));
				rest.setPr(rs.getString("pr"));
				rest.setFileName(rs.getString("file_name"));
				rest.setAreaId(rs.getInt("area_id"));
				restaurantList.add(rest);
			}

			return restaurantList;

		}catch(SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if (con != null) {
				con.close();
			}
		}
	}



	//レストラン総数を取得するメソッド
	public static double getRestaurantCountAll()throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("select count(*) as cnt from restaurant");
			ResultSet rs = st.executeQuery();
			double coung = 0.0;
			while(rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}

			return coung;
		}catch(SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//検索項目が全て入力されていた時のレストラン総数を取得するメソッド
	public static double getRestaurantCount(String name, int areaId, String phoneNum, String address)throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			Map<Integer, Object> params = new LinkedHashMap<>();
			Integer i = 1;
			con = DBManager.getConnection();
			StringBuilder sb = new StringBuilder("select count(*) as cnt from restaurant WHERE 1 = 1");
			//フォームに入力された項目ごとにSQL文を追加していく
			if(areaId != 0 && areaId != -1) {
				sb.append(" AND area_id = ? ");
				params.put(i,areaId);
				i++;
			}
			if(!name.equals("")) {
				sb.append(" AND name LIKE ? ");
				params.put(i, "%" + name + "%");
				i++;
			}
			if(!phoneNum.equals("")) {
				sb.append(" AND phone_number LIKE ? ");
				params.put(i, "%" + phoneNum + "%");
				i++;
			}
			if(!address.equals("")) {
				sb.append(" AND address LIKE ? ");
				params.put(i, "%" + address + "%");
				i++;
			}
			String sql = sb.toString();

			st = con.prepareStatement(sql);
			for(Integer key: params.keySet()) {
				int ph = key.intValue();
				st.setObject(ph, params.get(key));

			}
//			st = con.prepareStatement("select count(*) as cnt from restaurant where area_id=? and name LIKE ? "
//					+ " and phone_number LIKE ? and address LIKE ?");
//			st.setInt(1, areaId);
//			st.setString(2, "%" + name + "%");
//			st.setString(3, "%" + phoneNum + "%");
//			st.setString(4, "%" + address + "%");
			ResultSet rs = st.executeQuery();
			double coung = 0.0;
			while(rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}

			return coung;
		}catch(SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//キーワードからレストラン総数を取得するメソッド
	public static double getRestaurantCountByKeyWord(String keyWord)throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("select count(*) as cnt from restaurant where name like ?");
			st.setString(1, "%" + keyWord + "%");
			ResultSet rs = st.executeQuery();
			double coung = 0.0;
			while(rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}

			return coung;
		}catch(Exception e){
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//キーワードとエリアIDに一致するレストラン総数を取得するメソッド
	public static double getRestaurantCountById(String keyWord, int areaId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("select count(*) as cnt from restaurant where name like ? AND area_id = ?");
			st.setString(1, "%" + keyWord + "%");
			st.setInt(2, areaId);
			ResultSet rs = st.executeQuery();
			double coung = 0.0;
			while(rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}

			return coung;
		}catch(Exception e){
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if (con != null) {
				con.close();
			}
		}
	}


	//レストラン詳細を取得するメソッド
	public static RestaurantDataBeans getRestaurantById(int restaurantId) throws SQLException  {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM restaurant " +
					"INNER JOIN " +
					"area " +
					"ON " +
					"restaurant.area_id = area.id " +
					"WHERE restaurant.id = ?");
			st.setInt(1, restaurantId);

			ResultSet rs = st.executeQuery();

			RestaurantDataBeans rest = new RestaurantDataBeans();
			if(rs.next()) {
				rest.setId(rs.getInt("id"));
				rest.setName(rs.getString("name"));
				rest.setPhoneNumber(rs.getString("phone_number"));
				rest.setAddress(rs.getString("address"));
				rest.setDetail(rs.getString("detail"));
				rest.setPr(rs.getString("pr"));
				rest.setFileName(rs.getString("file_name"));
				rest.setAreaId(rs.getInt("area_id"));
				rest.setAreaName(rs.getString("area.name"));
			}

			System.out.println("searching restaurant by ID has been completed");
			return rest;
		}catch(SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//複合検索のメソッド
	public static ArrayList<RestaurantDataBeans> getRestaurant(int areaId, String name,
			String phoneNum, String address, int pageNum, int pageMaxItemCount) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			int startItemNum = (pageNum - 1) * pageMaxItemCount;
			Map<Integer, Object> params = new LinkedHashMap<>();
			Integer i = 1;
			con = DBManager.getConnection();
			StringBuilder sb = new StringBuilder("SELECT * FROM restaurant WHERE 1=1 ");
			if(areaId != 0 && areaId != -1) {
				sb.append(" AND area_id = ? ");
				params.put(i,areaId);
				i++;
			}
			if(!name.equals("")) {
				sb.append(" AND name LIKE ? ");
				params.put(i, "%" + name + "%");
				i++;
			}
			if(!phoneNum.equals("")) {
				sb.append(" AND phone_number LIKE ? ");
				params.put(i, "%" + phoneNum + "%");
				i++;
			}
			if(!address.equals("")) {
				sb.append(" AND address LIKE ? ");
				params.put(i, "%" + address + "%");
				i++;
			}
			params.put(i, startItemNum);
			i++;
			params.put(i, pageMaxItemCount);
			i++;
			sb.append(" ORDER BY area_id ASC LIMIT ?, ?");
			String sql = sb.toString();

			st = con.prepareStatement(sql);
			for(Integer key: params.keySet()) {
				int ph = key.intValue();
				st.setObject(ph, params.get(key));

			}
			ResultSet rs = st.executeQuery();
			ArrayList<RestaurantDataBeans> restaurantList = new ArrayList<RestaurantDataBeans>();

			while(rs.next()) {
				RestaurantDataBeans rest = new RestaurantDataBeans();
				rest.setId(rs.getInt("id"));
				rest.setName(rs.getString("name"));
				rest.setPhoneNumber(rs.getString("phone_number"));
				rest.setAddress(rs.getString("address"));
				rest.setDetail(rs.getString("detail"));
				rest.setPr(rs.getString("pr"));
				rest.setFileName(rs.getString("file_name"));
				rest.setAreaId(rs.getInt("area_id"));
				restaurantList.add(rest);
			}
			return restaurantList;
		}catch(SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static void restaurantDelete(int restaurantId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("DELETE FROM restaurant WHERE id =?");
			st.setInt(1, restaurantId);
			st.executeUpdate();
		}catch(SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static void restauranUpdate(RestaurantDataBeans restdb)throws SQLException {
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();

			if(restdb.getFileName() != null) {
				st = con.prepareStatement("UPDATE restaurant SET name = ? , phone_number = ? , address = ? ,  pr = ? , "
						+ " detail = ? , file_name = ?, area_id = ? WHERE id = ?");
				st.setString(1, restdb.getName());
				st.setString(2, restdb.getPhoneNumber());
				st.setString(3, restdb.getAddress());
				st.setString(4, restdb.getPr());
				st.setString(5, restdb.getDetail());
				st.setString(6, restdb.getFileName());
				st.setInt(7, restdb.getAreaId());
				st.setInt(8, restdb.getId());
				st.executeUpdate();
				System.out.println("update has been completed");
			}else{
				st = con.prepareStatement("UPDATE restaurant SET name = ? , phone_number = ? , address = ? ,  "
						+ "pr = ? , detail = ? , area_id = ? WHERE id = ?");
				st.setString(1, restdb.getName());
				st.setString(2, restdb.getPhoneNumber());
				st.setString(3, restdb.getAddress());
				st.setString(4, restdb.getPr());
				st.setString(5, restdb.getDetail());
				st.setInt(6, restdb.getAreaId());
				st.setInt(7, restdb.getId());
				st.executeUpdate();
				System.out.println("update has been completed");
			}
		}catch(SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//電話番号が重複していないかを確認するメソッド
	public static boolean isOverLapPhoneNum(String phoneNum) throws SQLException{
		// 重複しているかどうか表す変数
		boolean isOverlap = false;

		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			// 入力された電話番号が存在するか調べる
			st = con.prepareStatement("SELECT phone_number from restaurant WHERE phone_number = ?");
			st.setString(1, phoneNum);
			ResultSet rs = st.executeQuery();

			if(rs.next()) {
				isOverlap = true;
			}
			return isOverlap;
		}catch(SQLException e){
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}
	}

	public static void restaurantRegistration(RestaurantDataBeans restdb) throws SQLException{
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			if(restdb.getFileName() != null) {
				st = con.prepareStatement("INSERT INTO restaurant (name, phone_number, address, pr, detail, file_name, area_id) "
						+ " values(?, ?, ?, ?, ?, ?, ?)");
				st.setString(1, restdb.getName());
				st.setString(2, restdb.getPhoneNumber());
				st.setString(3, restdb.getAddress());
				st.setString(4, restdb.getPr());
				st.setString(5, restdb.getDetail());
				st.setString(6, restdb.getFileName());
				st.setInt(7, restdb.getAreaId());
				st.executeUpdate();
			}else{
				st = con.prepareStatement("INSERT INTO restaurant (name, phone_number, address, pr, detail, area_id) "
						+ " values(?, ?, ?, ?, ?, ?)");
				st.setString(1, restdb.getName());
				st.setString(2, restdb.getPhoneNumber());
				st.setString(3, restdb.getAddress());
				st.setString(4, restdb.getPr());
				st.setString(5, restdb.getDetail());
				st.setInt(6, restdb.getAreaId());
				st.executeUpdate();
			}

		}catch(SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if (con != null) {
				con.close();
			}
		}
	}
}
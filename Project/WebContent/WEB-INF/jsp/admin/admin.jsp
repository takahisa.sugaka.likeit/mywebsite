<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>管理画面</title>
        <link rel="stylesheet"
              href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">
        <link href="css/admin_style.css" rel="stylesheet" type="text/css" />
    </head>

    <body>
        <jsp:include page="/baselayout/admin_header.jsp" />

        <div class="container">
            <div class="row mt-4">
                <h4 class="col-md-12 text-center reservelist-header">管理画面</h4>
            </div>
            <div class="row">
                <a class="offset-11" href="RestaurantNewRegistration">新規登録</a>
            </div>
            <div class="row">
                <div class="col-md-12">
                	<c:if test="${validationMessage != null}">
						<P class="text-danger mx-auto">${validationMessage}</P>
					</c:if>
                    <div class="card reserve-confirm mb-5">
                        <div class="card-content">
                            <div class="form-group">
								<form action="" method="get" action="Admin">
									<div class="row  mt-1">
                                        <div class="col-md-6 col-sm-6">
                                            <label class="ml-1">地域・駅</label>
                                            <input type="text" name="area" value="${area}" class="form-control">
                                        </div>
                                        <div class=" col-md-6 col-sm-6">
                                            <label class="ml-1">店舗名</label>
                                            <input type="text" name="restaurant_name"  value="${name}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-md-6">
                                            <label class="ml-1">電話番号</label>
                                            <input type="text" name="phone_number" value="${phoneNum}" class="form-control">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="ml-1">住所</label>
                                            <input type="text" name="address"  value="${address}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 mt-3">
                                            <button class="btn btn-secondary col-md-4 offset-md-4" type="submit">検索</button>
                                        </div>
                                    </div>
                       	　　		</form>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
            	<div class="mx-auto">
	            	<c:forEach var="rest" items="${restaurantList}">
						<div class="restaurant-module">
							<div class="card mb-3" style="max-width: 650px;">
						    	<div class="row no-gutters">
									<div class="col-md-4">
										<img src="img/${rest.fileName}" class="card-img" alt="...">
									</div>
									<div class="col-md-8">
								    	<div class="card-body">
											<h5 class="restaurant-name">${rest.name}</h5>
											<p class="restaurant-description">${rest.pr}</p>
											<p class="restaurant-area">住所：${rest.address}</p>
											<p class="restaurant-tel">TEL：${rest.phoneNumber}</p>
										</div>
	                                    <div class="btn-area text-center">
		                                    <a class=" btn btn-primary" href="RestaurantDetail?id=${rest.id}">詳細</a>
		                                    <a class=" btn btn-success" href="RestaurantUpdate?id=${rest.id}">更新</a>
		                                    <a class=" btn btn-danger" href="RestaurantDeleteConfirm?id=${rest.id}">削除</a>
	                                    </div>
	                            	</div>
	                        	</div>
	                    	</div>
	                	</div>
	             	</c:forEach>
             	</div>
             </div>
            <div class="row">
				<div class="col-offset-5 col-md-2 mx-auto" >
					<nav aria-label="Page navigation example">
						<ul class="pagination">

						<!-- 1ページ戻るボタン -->
						<c:choose>
							<c:when test="${pageNum == 1}">
							    <li class="page-item disabled">
									<a class="page-link" href="" aria-label="Previous">
									    <span aria-hidden="true">&laquo;</span>
									    <span class="sr-only">Previous</span>
									</a>
							    </li>
							</c:when>
						    <c:otherwise>
							    <li class="page-item">
									<a class="page-link" href="Admin?area=${area}&restaurant_name=${name}&phone_number=${phoneNum}&address=${address}&page_num=${pageNum - 1}" aria-label="Previous">
									    <span aria-hidden="true">&laquo;</span>
										<span class="sr-only">Previous</span>
									</a>
								</li>
							</c:otherwise>
						</c:choose>

						<!-- ページインデックス -->
						<c:forEach begin="${(pageNum - 5) > 0 ? pageNum - 5 : 1}" end="${(pageNum + 5) > pageMax ? pageMax : pageNum + 5}" step="1" varStatus="status">
							<li class="page-item" <c:if test="${pageNum == status.index }"> class="active" </c:if>><a class="page-link" href="Admin?area=${area}&restaurant_name=${name}&phone_number=${phoneNum}&address=${address}&page_num=${status.index}">${status.index}</a></li>
						</c:forEach>

						<!-- 1ページ進むボタン -->
						<c:choose>
							<c:when test="${pageNum == pageMax || pageMax == 0}">
								<li class="page-item disabled">
									<a class="page-link" href="#" aria-label="Next">
										<span aria-hidden="true">&raquo;</span>
										<span class="sr-only">Next</span>
								    </a>
							    </li>
							</c:when>
						    <c:otherwise>
							    <li class="page-item">
								    <a class="page-link" href="Admin?area=${area}&restaurant_name=${name}&phone_number=${phoneNum}&address=${address}&page_num=${pageNum + 1}" aria-label="Next">
									    <span aria-hidden="true">&raquo;</span>
										<span class="sr-only">Next</span>
								    </a>
								</li>
							</c:otherwise>
					    </c:choose>
						</ul>
					</nav>
			    </div>
			</div>
        </div>
    </body>
</html>
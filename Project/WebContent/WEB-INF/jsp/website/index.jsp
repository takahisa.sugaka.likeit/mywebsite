<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
	<head>
	    <meta charset="UTF-8">
	    <title>グルメ検索</title>
	    <link rel="stylesheet"
	          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	          crossorigin="anonymous">
	    <link href="css/style.css" rel="stylesheet" type="text/css" />
	</head>

    <body>
        <jsp:include page="/baselayout/header.jsp" />

        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="search">
                            <form class="search-form form-inline" action="SearchResult">
                                <input type="text" placeholder="エリア" name="area" class="form-control" />
                                <input type="text" placeholder="キーワード" name="keyword" class="form-control" />
                                <button class="form-control">検索</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="restaurants-area mt-4">
                <h3>おすすめ</h3>
                <div class="card-deck mt-1">
                	<c:forEach var="rest" items="${restaurantList}">
                    <div class="card" style="width: 20rem;">
                        <img class="card-img-top" src="img/${rest.fileName}" alt="Card image cap">
                        <div class="card-body">
                            <h4 class="card-title">${rest.name}</h4>
                            <p class="card-text">${rest.pr}</p>
                            <a class="btn btn-orange" href="Restaurant?restaurant_id=${rest.id}">詳細へ</a>
                        </div>
                    </div>
                    </c:forEach>
                </div>
            </div>
        </div>
    </body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>ユーザー情報更新完了</title>
        <link rel="stylesheet"
              href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">
        <link href="css/style.css" rel="stylesheet" type="text/css" />
    </head>

    <body>
        <jsp:include page="/baselayout/header.jsp" />

        <div class="container">
            <div class="row mt-4">
                <h4 class="col-md-12 text-center reservelist-header">更新完了</h4>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card reserve-confirm mb-5">
                        <div class="card-content">
                            <div class="form-group">
                                <form action="" method="POST">
                                    <div class="row  mt-1">
                                        <div class="col-md-6 col-sm-6">
                                            <label class="ml-1">名前</label>
                                            <p class="ml-1 border-bottom">${udb.name}</p>
                                        </div>
                                        <div class=" col-md-6 col-sm-6">
                                            <label class="ml-1">ログインID</label>
                                            <p class="ml-1 border-bottom">${udb.loginId}</p>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <label class="ml-1">メールアドレス</label>
                                            <p class="ml-1 border-bottom">${udb.mailAddress}</p>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <p class="text-center">上記内容で更新しました</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 mt-3">
                                            <a class="btn btn-success col-md-4 offset-md-4" href="UserPage">ユーザー情報へ</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
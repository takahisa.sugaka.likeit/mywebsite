<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>ユーザー情報/更新確認</title>
        <link rel="stylesheet"
              href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">
        <link href="css/style.css" rel="stylesheet" type="text/css" />
    </head>

    <body>
        <jsp:include page="/baselayout/header.jsp" />

        <div class="container">
            <div class="row mt-4">
                <h4 class="col-md-12 text-center reservelist-header">入力内容確認</h4>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card reserve-confirm mb-5">
                        <div class="card-content">
                            <div class="form-group">
                                <form action="UserUpdateResult" method="POST">
                                    <div class="row  mt-1">
                                        <div class="col-md-6 col-sm-6">
                                            <label class="ml-1">名前</label>
                                            <input type="text" name="user_name_update" value="${udb.name}" class="form-control" readonly>
                                        </div>
                                        <div class=" col-md-6 col-sm-6">
                                            <label class="ml-1">ログインID</label>
                                            <input type="text" name="login_id_update" value="${udb.loginId}" class="form-control" readonly>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <label class="ml-1">メールアドレス</label>
                                            <input type="text" name="user_mail_address_update" value="${udb.mailAddress}" class="form-control" readonly>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <p class="text-center">上記内容で更新してよろしいでしょうか?</p>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-md-6 col-sm-6">
                                            <button class="btn btn-success col-md-4 offset-md-4" type="submit" name="confirmButton" value="cancel">戻る</button>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <button class="btn btn-success col-md-4 offset-md-4" type="submit" name="confirmButton" value="update">更新</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
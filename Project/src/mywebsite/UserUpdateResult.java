package mywebsite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class UserUpdateResult
 */
@WebServlet("/UserUpdateResult")
public class UserUpdateResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateResult() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			//入力フォームの値をUserDateBeansにセット
			UserDataBeans udb = new UserDataBeans();
			udb.setUpdateUserDataBeansInfo(request.getParameter("user_name_update"), request.getParameter("login_id_update"), request.getParameter("user_mail_address_update"), (int)session.getAttribute("userId"));

			//押されたボタンを確認する
			String confirmed = request.getParameter("confirmButton");
			//押されたボタンによって処理が分岐
			switch(confirmed) {
			//戻るボタンが押されていたら、セッションに内容を入れてマイページへ戻る
			case "cancel":
				session.setAttribute("returnUDB", udb);
				response.sendRedirect("UserPage");
				break;
			//更新ボタンが押されていたら、update処理を行い完了画面へ遷移
			case "update":
				UserDAO.updateUser(udb);
				request.setAttribute("udb", udb);
				request.getRequestDispatcher(WebHelper.USER_UPDATA_RESULT_PAGE).forward(request, response);
			}

		}catch(Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");

		}
	}

}

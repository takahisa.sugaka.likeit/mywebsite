<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>ユーザー登録</title>
        <link rel="stylesheet"
              href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">
        <link href="css/style.css" rel="stylesheet" type="text/css" />
    </head>

    <body>
    	<jsp:include page="/baselayout/header.jsp" />

        <div class="container">
            <div class="row mt-4">
                <h4 class="col-md-12 text-center reservelist-header">ユーザー登録</h4>
                <c:if test="${validationMessage != null}">
					<P class="text-danger mx-auto">${validationMessage}</P>
				</c:if>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card reserve-confirm mb-5">
                        <div class="card-content">
                            <div class="form-group">
                                <form action="RegistConfirm" method="POST">
                                    <div class="row  mt-3">
                                        <div class="col-md-12">
                                            <label class="ml-1">名前</label>
                                            <input type="text" name="user_name" value="${udb.name}" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class=" col-md-12">
                                            <label class="ml-1">ログインID</label>
                                            <input type="text" name="login_id" value="${udb.loginId}" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <label class="ml-1">メールアドレス</label>
                                            <input type="email" name="user_mail_address" value="${udb.mailAddress}" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class=" col-md-12">
                                            <label class="ml-1">メールアドレス(確認)</label>
                                            <input type="email" name="user_mail_address_confirm" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class=" col-md-12">
                                            <label class="ml-1">パスワード</label>
                                            <input type="password" name="password" class="form-control" required>
                                        </div>
                                    </div>
                                     <div class="row mt-3">
                                        <div class=" col-md-12">
                                            <label class="ml-1">パスワード(確認)</label>
                                            <input type="password" name="password_confirm" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <button class="btn btn-success col-md-4 offset-md-4" type="submit">登録</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
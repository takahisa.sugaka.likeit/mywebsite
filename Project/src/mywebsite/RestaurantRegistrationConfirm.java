package mywebsite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import beans.RestaurantDataBeans;
import dao.AreaDAO;
import dao.RestaurantDAO;

/**
 * Servlet implementation class RestaurantRegistrationConfirm
 */
@WebServlet("/RestaurantRegistrationConfirm")
@MultipartConfig(location = "/Users/Takahisa/Documents/git/MyWebSite/Project/WebContent/tmp", maxFileSize = 1048576)
public class RestaurantRegistrationConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RestaurantRegistrationConfirm() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {
			//セッションからユーザーidを取得
			int userId = (int) session.getAttribute("userId");

			//管理者アカウントでログインしていなかったらリダイレクト
			if (userId != WebHelper.ADMIN_ID) {
				response.sendRedirect("Index");
				return;
			}
			//エラーメッセージを格納する変数
			String validationMessage = "";

			//RestaurantDataBeansに入れたいものの値を適宜変換しておく
			System.out.println(request.getParameter("area_id"));
			int areaId = Integer.parseInt(request.getParameter("area_id"));
			String areaName = AreaDAO.getAreaNameById(areaId);

			//電話番号がMySQLに登録済みかどうかをチェック
			if (RestaurantDAO.isOverLapPhoneNum(request.getParameter("phone_number"))) {
				validationMessage += "すでに登録されている電話番号です。<br>";
			}

			RestaurantDataBeans restdb = new RestaurantDataBeans();
			//partインスタンスを生成
			Part part = request.getPart("photo");
			String photoName = WebHelper.getFileName(part);
			//画像がアップロードされていた時の処理
			if(!photoName.equals("")) {
				part.write(photoName);
				restdb.setFileName(photoName);
				System.out.println("画像がアップロードされています");
			}

			//beansに各種値をセットする
			restdb.setName(request.getParameter("restaurant_name"));
			restdb.setPhoneNumber(request.getParameter("phone_number"));
			restdb.setAddress(request.getParameter("address"));
			restdb.setPr(request.getParameter("pr"));
			restdb.setDetail(request.getParameter("detail"));
			restdb.setAreaId(areaId);
			restdb.setAreaName(areaName);

			//ValidationMessageにメッセージがなかったら確認画面へ遷移
			if (validationMessage.length() == 0) {
				session.setAttribute("restdbS", restdb);
				session.setAttribute("part", part);
				request.getRequestDispatcher(WebHelper.RESTAURANT_REGISTRATION_CONFIRM).forward(request, response);
			} else {
				//セッションにエラーメッセージを格納して新規登録画面へ
				session.setAttribute("validationMessage", validationMessage);
				response.sendRedirect("RestaurantNewRegistration");
			}
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}

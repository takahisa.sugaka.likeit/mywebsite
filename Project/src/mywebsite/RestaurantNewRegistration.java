package mywebsite;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.AreaDataBeans;
import beans.RestaurantDataBeans;
import dao.AreaDAO;

/**
 * Servlet implementation class NewRegistration
 */
@WebServlet("/RestaurantNewRegistration")
public class RestaurantNewRegistration extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RestaurantNewRegistration() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			//セッションからユーザーidを取得
			int userId = (int)session.getAttribute("userId");

			//管理者アカウントでログインしていなかったらリダイレクト
			if(userId != WebHelper.ADMIN_ID) {
				response.sendRedirect("Index");
				return;
			}

			//セッション領域のレストランBeansを削除
			session.removeAttribute("restdb");

			RestaurantDataBeans restdbS =  session.getAttribute("restdbS")!= null ? (RestaurantDataBeans) WebHelper.cutSessionAttribute(session,"restdbS"): null;
			ArrayList<AreaDataBeans> areaList = new ArrayList<AreaDataBeans>();

			//エラーメッセージがあったときにエラーメッセージを格納する
			String validationMessage = (String) WebHelper.cutSessionAttribute(session, "validationMessage");
			request.setAttribute("validationMessage", validationMessage);

			if(restdbS == null) {
				areaList = AreaDAO.getAreaList();
				request.setAttribute("areaList", areaList);
			}else {
				areaList = AreaDAO.getAreaListExclusionRestaurantLocation(restdbS.getAreaId());
				request.setAttribute("areaList", areaList);
				request.setAttribute("restdb", restdbS);
			}

			request.getRequestDispatcher(WebHelper.NEW_REGISTER).forward(request, response);
		}catch(Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}

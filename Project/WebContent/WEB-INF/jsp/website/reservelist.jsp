<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
	<head>
	    <meta charset="UTF-8">
	    <title>予約一覧</title>
	    <link rel="stylesheet"
	          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	          crossorigin="anonymous">
	    <link href="css/style.css" rel="stylesheet" type="text/css" />
	</head>

	<body>
	    <jsp:include page="/baselayout/header.jsp" />

	    <div class="container">
	        <div class="row">
	            <div class="col-md-12 mt-3">
	                <div class="reservelist-header-area">
	                    <h4 class="reservelist-header text-center">
	                        予約一覧
	                    </h4>
	                </div>
	            </div>
	        </div>
	       	<div class="row">
	       	<c:forEach var="rl" items="${rl}">
	       		<div class="mx-auto mt-3 ">
	                <div class="reserve-list">
	                    <div class="card mb-3" style="max-width: 650px;">
	                        <div class="row no-gutters">
	                            <div class="col-md-4">
	                                <img src="img/${rl.fileName}" class="card-img" alt="...">
	                            </div>
	                            <div class="col-md-8">
	                                <div class="card-body">
	                                    <h5 class="restaurant-name">${rl.restaurantName}</h5>
	                                    <p class="reserve-date d-inline" >予約日時：${rl.formatDate}${rl.reserveTime}</p>
	                                    <a class="btn btn-orange btn-reservelist mt-1" href="ReserveDetail?reserve_id=${rl.id}">予約詳細</a>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	       	</c:forEach>
	       	</div>

	        <div class="row">
	            <div class="col-md-12 mt-3">
	                <div class="reservelist-header-area">
	                    <h4 class="reservelist-header text-center">
	                        予約履歴
	                    </h4>
	                </div>
	            </div>
	        </div>

	        <div class="row">
	       	<c:forEach var="prl" items="${prl}">
	       		<div class="mx-auto mt-3 ">
	                <div class="reserve-list">
	                    <div class="card mb-3" style="max-width: 650px;">
	                        <div class="row no-gutters">
	                            <div class="col-md-4">
	                                <img src="img/${prl.fileName}" class="card-img" alt="...">
	                            </div>
	                            <div class="col-md-8">
	                                <div class="card-body">
	                                    <h5 class="restaurant-name">${prl.restaurantName}</h5>
	                                    <p class="reserve-date d-inline" >予約日時：${prl.formatDate}${prl.reserveTime}</p>
	                                    <c:choose>
	                                    	<c:when test="${prl.cancel == true}">
	                                    		<p class="d-inline bg-dark text-white">キャンセル</p>
	                                    	</c:when>
	                                    	<c:otherwise>
	                                    		<p class="d-inline bg-dark text-white">来店済み</p>
	                                    	</c:otherwise>
	                                    </c:choose>
	                                    <a class="btn btn-orange btn-reservelist mt-3" href="ReserveDetail?reserve_id=${prl.id}">予約詳細</a>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	       	</c:forEach>
	       	</div>
	    </div>
	</body>
</html>